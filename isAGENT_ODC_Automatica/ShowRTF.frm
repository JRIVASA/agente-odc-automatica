VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RichTx32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form ShowRTF 
   BackColor       =   &H00808080&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " "
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   Icon            =   "ShowRTF.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox CoolBar2 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   11850
      TabIndex        =   8
      Top             =   8250
      Width           =   11910
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Caption         =   "Frame3"
         Height          =   315
         Left            =   30
         TabIndex        =   12
         Top             =   30
         Width           =   11790
         Begin VB.CommandButton Command3 
            CausesValidation=   0   'False
            Height          =   270
            Index           =   0
            Left            =   945
            Picture         =   "ShowRTF.frx":628A
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   15
            Width           =   285
         End
         Begin VB.CommandButton Command3 
            CausesValidation=   0   'False
            Height          =   270
            Index           =   1
            Left            =   2475
            Picture         =   "ShowRTF.frx":6594
            Style           =   1  'Graphical
            TabIndex        =   14
            Top             =   15
            Width           =   285
         End
         Begin VB.TextBox Text1 
            Height          =   285
            Left            =   1230
            Locked          =   -1  'True
            TabIndex        =   13
            Text            =   " "
            Top             =   15
            Width           =   1215
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "P�ginas"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000002&
            Height          =   210
            Left            =   120
            TabIndex        =   16
            Top             =   30
            Width           =   750
         End
      End
   End
   Begin VB.PictureBox CoolBar1 
      Align           =   1  'Align Top
      Height          =   525
      Left            =   0
      ScaleHeight     =   465
      ScaleWidth      =   11850
      TabIndex        =   7
      Top             =   0
      Width           =   11910
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   465
         Left            =   30
         TabIndex        =   9
         Top             =   30
         Width           =   11790
         Begin VB.CommandButton Command2 
            Caption         =   "Imprimir Pagina"
            Height          =   435
            Index           =   0
            Left            =   600
            TabIndex        =   11
            Top             =   0
            Visible         =   0   'False
            Width           =   1575
         End
         Begin VB.CommandButton Command2 
            Height          =   450
            Index           =   1
            Left            =   0
            Picture         =   "ShowRTF.frx":689E
            Style           =   1  'Graphical
            TabIndex        =   10
            Top             =   -15
            Width           =   570
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Campos del Documento"
      Height          =   435
      Left            =   6375
      TabIndex        =   2
      Top             =   8235
      Width           =   4095
      Begin VB.ListBox L1 
         Height          =   645
         Index           =   0
         Left            =   240
         Sorted          =   -1  'True
         TabIndex        =   4
         Top             =   480
         Width           =   1935
      End
      Begin VB.ListBox L1 
         Height          =   645
         Index           =   1
         Left            =   2280
         TabIndex        =   3
         Top             =   480
         Width           =   1935
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   " Detalle"
         Height          =   375
         Index           =   0
         Left            =   2280
         TabIndex        =   6
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Maestro"
         Height          =   375
         Index           =   0
         Left            =   885
         TabIndex        =   5
         Top             =   1695
         Width           =   1335
      End
   End
   Begin MSComDlg.CommonDialog Com 
      Left            =   9270
      Top             =   1590
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComDlg.CommonDialog Co 
      Left            =   5040
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin RichTextLib.RichTextBox Ri 
      Height          =   495
      Left            =   135
      TabIndex        =   0
      Top             =   8535
      Visible         =   0   'False
      Width           =   10980
      _ExtentX        =   19368
      _ExtentY        =   873
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"ShowRTF.frx":7008
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin RichTextLib.RichTextBox Ri2 
      Height          =   7470
      Left            =   75
      TabIndex        =   1
      Top             =   630
      Width           =   11745
      _ExtentX        =   20717
      _ExtentY        =   13176
      _Version        =   393217
      BorderStyle     =   0
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   2
      Appearance      =   0
      TextRTF         =   $"ShowRTF.frx":7082
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Shape Shape1 
      FillColor       =   &H00FFFFFF&
      FillStyle       =   0  'Solid
      Height          =   7560
      Left            =   60
      Top             =   600
      Width           =   11800
   End
End
Attribute VB_Name = "ShowRTF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command2_Click(Index As Integer)
    
    On Error GoTo ERROR1
    
    Select Case Index
        Case 0
            Rep.PrintFile True
        Case 1
            Rep.PrintAll True
    End Select
    
    Text1.Text = Rep.page
    
    Exit Sub
    
ERROR1:

End Sub

Private Sub Command3_Click(Index As Integer)
    
    Select Case Index
        Case 0
            Rep.PreviousPage
        Case 1
            Rep.NextPage
    End Select
    
    Text1.Text = Rep.page
    
End Sub

Private Sub Form_Load()
    Frame2.Visible = False
End Sub

Public Sub LoadFile(path As String)
    
    Select Case UCase(find_concept)
        Case "VEN", "FACE"
            If find_status = "DWT" Then
            Else
                path = IIf(Len(Trim(str_facturaRTF)) = 0, path, str_facturaRTF)
            End If
        Case "NDE", "NDEE"
            If find_status = "DWT" Then
            Else
                path = IIf(Len(Trim(str_ndeRTF)) = 0, path, str_ndeRTF)
            End If
        Case "DEV"
            If find_status = "DWT" Then
            Else
                path = IIf(Len(Trim(str_DevolucionRTF)) = 0, path, str_DevolucionRTF)
            End If
        Case "PED"
            If find_status = "DWT" Then
            Else
                path = IIf(Len(Trim(str_PedidoRTF)) = 0, path, str_PedidoRTF)
            End If
        Case "PRE"
            If find_status = "DWT" Then
            Else
                path = IIf(Len(Trim(str_PresupuestoRTF)) = 0, path, str_PresupuestoRTF)
            End If
        Case "HBL"
            If find_status = "DWT" Then
            Else
                path = IIf(Len(Trim(str_PresupuestoRTF)) = 0, path, str_PresupuestoRTF)
            End If
    End Select
    
    Rep.SetRTBandCML Ri, Ri2, Co
    'Ri1.Visible = False
    Rep.OpenFile path, Rtf:=True
    
    If Rep.Procesar Then
        Command3_Click (0)
    End If
    
End Sub

Public Sub PrintAllPage(Optional blnDialogoImprimir As Boolean = True)
    '  Rep.SetRTBandCML Ri, Ri2, Co
    '  Rep.OpenFile (path), Rtf:=True
    '  Rep.Procesar
    'old
    'Rep.PrintAll blnDialogoImprimir
    Rep.PrintAlltoPDF
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set Rep = Nothing
End Sub

