VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsExtendidaReporte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mConexion As ADODB.Connection

Public Sub IniciarCls(pCn As ADODB.Connection)
    Set mConexion = pCn
End Sub

Public Function SeleccionReporteExt() As Boolean
    SQL = "SELECT * FROM MA_PLANTILLAS WHERE  (imagen = 'Plantilla')"
    Dim rstmp As New ADODB.Recordset
    rstmp.Open SQL, mConexion, adOpenForwardOnly, adLockReadOnly
    SeleccionReporteExt = False
    If Not rstmp.EOF Then
        frp_ReportesExt.Show vbModal
        SeleccionReporteExt = frp_ReportesExt.fTipoReporte
    
    End If
End Function

Public Function usarReporteExt(documento As String, TablaTr As String, _
concepto As String) As Boolean
    
    Dim sqlWhere As String
    SQL = "SELECT * FROM MA_PLANTILLAS WHERE  (imagen = 'Plantilla') AND TIPOPRODUCTO = 'ZAPATOS'"
    Dim rstmp As New ADODB.Recordset
    Dim rsExt As New ADODB.Recordset
    
    rstmp.Open SQL, mConexion, adOpenForwardOnly, adLockReadOnly
    usarReporteExt = False
     Select Case UCase(concepto)
     
     Case Is = "ODC"
        
        sqlWhere = ""
     Case Else
        sqlWhere = ""
     End Select
    If Not rstmp.EOF Then
        '
        sql2 = " select COUNT (*) as cant from (" _
            & " select MA_PRODUCTOS.C_DESCRI_BASE as Resultado from MA_PRODUCTOS " _
            & " inner join " & TablaTr & " on " & TablaTr & ".c_codArticulo = ma_productos.c_Codigo " _
            & " where " & TablaTr & ".c_DOCUMENTO = '" & documento & "' " & sqlWhere & " " _
            & " ) as tb1 where tb1.Resultado <>'' "
            
         
            rsExt.Open sql2, mConexion, adOpenForwardOnly, adLockReadOnly
            If Not rsExt.EOF Then
               'frp_ReportesExt.Show vbModal
               'usarReporteExt = frp_ReportesExt.fTipoReporte
               usarReporteExt = True
            End If
            
    End If
    
End Function

Public Function ORDER_PLANTILLA()
    SQL = "SELECT * FROM MA_PLANTILLAS WHERE  (imagen = 'Plantilla')"
    Dim Farreglos_order()
    Dim rstmp As New ADODB.Recordset
    Dim rstmp1 As New ADODB.Recordset
    rstmp.CursorLocation = adUseServer
    rstmp.Open SQL, mConexion, adOpenForwardOnly, adLockReadOnly
    kw = 0
    While Not rstmp.EOF
        DoEvents
        sql1 = "SELECT * From MA_PLANTILLAS WHERE (imagen = 'Cuenta') AND (relacion = '" & rstmp!Clave & "') ORDER BY relacion,  prioridad,clave"
        rstmp1.Open sql1, mConexion, adOpenForwardOnly, adLockReadOnly
        mCampos = "C_DESCRI_BASE"
        mNOMBRECampos = "PRODUCTO"
        i = 0
        mBoolean = True
        While Not rstmp1.EOF And mBoolean
            mNOMBRECampos = mNOMBRECampos & "," & rstmp1!Texto
            mCampos = mCampos & "," & rstmp1!Campo
            rstmp1.MoveNext
            i = i + 1
            If i = 3 Then
                mBoolean = False
            End If
        Wend
        rstmp1.Close
        kw = kw + 1
        ReDim Preserve Farreglos_order(kw)
        Farreglos_order(kw) = Array(mNOMBRECampos, mCampos)
        rstmp.MoveNext
    Wend
    ORDER_PLANTILLA = Farreglos_order
End Function

Public Sub reporteExtendido(pRsCorreo As ADODB.Recordset, pNoDoc As String, pImpresoraoPantalla, TablaTr As String, concepto As String)
        On Error GoTo erro1
    
    Dim mRs As New ADODB.Recordset
    Dim mRsVer As New ADODB.Recordset
    Dim mCamposFijos
    Dim mNumColRep
    Dim mEmpresa As String
    
    mNumColRep = 17
    mValor = ORDER_PLANTILLA
    Set gClsDatos = CreateObject("recsun.cls_datos")
    mEmpresa = BuscarNombreEmpresa
    Select Case UCase(concepto)
    
    Case "INV"
    SQL = " SELECT     " & TablaTr & ".c_DOCUMENTO, " & TablaTr & ".c_CODARTICULO, " & TablaTr & ".n_CANTIDAD, " & TablaTr & ".n_COSTO, " & TablaTr & ".n_subtotal," & TablaTr & ".n_subtotal as n_total, 0 as n_impuesto ,  " & _
                          "  MA_PRODUCTOS.C_DESCRI, MA_PRODUCTOS.c_marca, MA_PRODUCTOS.c_modelo, MA_PRODUCTOS.C_CODIGO_BASE, " & _
                          " MA_PRODUCTOS.C_DESCRI_BASE, MA_PRODUCTOS.TEXT1, MA_PRODUCTOS.TEXT2, MA_PRODUCTOS.TEXT3, MA_PRODUCTOS.TEXT4, " & _
                          " MA_PRODUCTOS.TEXT5, MA_PRODUCTOS.TEXT6, MA_PRODUCTOS.TEXT7, MA_PRODUCTOS.TEXT8, MA_PRODUCTOS.DATE1, " & _
                          " MA_PRODUCTOS.NUME1, MA_PRODUCTOS.N_CANTIDAD_TMP, MA_PRODUCTOS.C_COD_PLANTILLA, MA_PRODUCTOS.N_PROD_EXT, " & _
                          " MA_PRODUCTOS.N_PRO_EXT,MA_PRODUCTOS.C_DESCRI_BASE as Resultado " & _
    " FROM         " & TablaTr & " INNER JOIN " & _
                          " MA_PRODUCTOS ON " & TablaTr & ".c_CODARTICULO = MA_PRODUCTOS.C_CODIGO "
    Case Else
            SQL = " SELECT     " & TablaTr & ".c_DOCUMENTO, " & TablaTr & ".c_CODARTICULO, " & TablaTr & ".n_CANTIDAD, " & TablaTr & ".n_COSTO, " & TablaTr & ".n_subtotal, " & TablaTr & ".n_impuesto, " & _
                          " " & TablaTr & ".n_total, MA_PRODUCTOS.C_DESCRI, MA_PRODUCTOS.c_marca, MA_PRODUCTOS.c_modelo, MA_PRODUCTOS.C_CODIGO_BASE, " & _
                          " MA_PRODUCTOS.C_DESCRI_BASE, MA_PRODUCTOS.TEXT1, MA_PRODUCTOS.TEXT2, MA_PRODUCTOS.TEXT3, MA_PRODUCTOS.TEXT4, " & _
                          " MA_PRODUCTOS.TEXT5, MA_PRODUCTOS.TEXT6, MA_PRODUCTOS.TEXT7, MA_PRODUCTOS.TEXT8, MA_PRODUCTOS.DATE1, " & _
                          " MA_PRODUCTOS.NUME1, MA_PRODUCTOS.N_CANTIDAD_TMP, MA_PRODUCTOS.C_COD_PLANTILLA, MA_PRODUCTOS.N_PROD_EXT, " & _
                          " MA_PRODUCTOS.N_PRO_EXT,MA_PRODUCTOS.C_DESCRI_BASE as Resultado " & _
    " FROM         " & TablaTr & " INNER JOIN " & _
                          " MA_PRODUCTOS ON " & TablaTr & ".c_CODARTICULO = MA_PRODUCTOS.C_CODIGO "
        
    
    
    End Select
    
    Select Case UCase(concepto)
    Case "ODC"
        sqlWhere = " WHERE     (" & TablaTr & ".c_DOCUMENTO = '" & pNoDoc & "') "
    Case Else
        If UCase(TablaTr) = "TR_INVENTARIO" Then
            sqlWhere = " WHERE     (" & TablaTr & ".c_DOCUMENTO = '" & pNoDoc & "' and " & TablaTr & ".c_concepto = '" & concepto & "' ) "
        Else
            sqlWhere = " WHERE     (" & TablaTr & ".c_DOCUMENTO = '" & pNoDoc & "') "
        End If
    End Select
    
    
    
    SqlOrder = " ORDER BY " & mValor(1)(1)
    
    Debug.Print SQL & sqlWhere & SqlOrder
    mCamposFijos = Split(mValor(1)(1), ",")
    mCamposFijosDes = Split(mValor(1)(0), ",")
    mLonCamposFijos = UBound(mCamposFijos)
    mCampoVerticalizar = mCamposFijos(mLonCamposFijos)
    
    ReDim Preserve mCamposFijos(mLonCamposFijos - 1)
    'mCamposValores = Array("Resultado", "n_Subtotal", "n_Cantidad", "n_costo")
    mCamposValores = Array("Resultado", "n_Cantidad", "n_costo")
    'Debug.Print sql & sqlWhere & SqlOrder
    Set mRs = mConexion.Execute(SQL & sqlWhere & SqlOrder)
    mValor = gClsDatos.Verticalizar(mRs, mCamposFijos, CStr(mCampoVerticalizar), 50, mCamposValores)
    Set mRsVer = mValor(0)
    
    MARRA = mValor(1)
    mmarraOrd = MARRA
    mmarraPosReal = MARRA
    mLonarrMarra = UBound(MARRA)
    Set gclsbusord_array = CreateObject("recsun.cls_busquedas_ordenamiento_array")
    gclsbusord_array.Ordenar_valorArray_Quicksort mmarraOrd, 0, mLonarrMarra
    For i = 0 To mLonarrMarra
        mmarraPosReal(i) = gclsbusord_array.buscar_valorArray(MARRA, mmarraOrd(i))
    Next
    
    'mRsVer.Fields.Append "SUMRESULTADO", adVarChar, 20, adFldUpdatable
    
    Dim mRsVer2 As ADODB.Recordset
    
    Set mRsVer2 = MakeRS(mRsVer)
    
    mRsVer2.Fields.Append "SumResultado", adVarWChar, 15, adFldUpdatable
        
    Call OpenAndFillRS(mRsVer, mRsVer2)
    
    If Not mRsVer2.EOF Then
        mRsVer2.MoveFirst
        While Not mRsVer2.EOF
        
            SUMCANTIDAD = 0#
            SUMCOSTO = 0#
        
            For i = 0 To 50
                
                mRsVer2("Resultado" & i) = mRsVer2("n_Cantidad" & i) & vbNewLine & FormatNumber(mRsVer2("n_costo" & i), 2)
                
                SUMCANTIDAD = SUMCANTIDAD + IIf(IsEmpty(mRsVer2("n_Cantidad" & i)), 0, CDbl(mRsVer2("n_Cantidad" & i)))

                If IsEmpty(mRsVer2("n_Costo" & i)) Then
                SUMCOSTO = SUMCOSTO + 0
                Else
                SUMCOSTO = SUMCOSTO + (CDbl(FormatNumber(mRsVer2("n_costo" & i), 2)) * CDbl(FormatNumber(mRsVer2("n_cantidad" & i), 2)))
                End If
                                
            Next
            
            DoEvents
            
            mRsVer2("SumResultado") = CStr(SUMCANTIDAD) & vbNewLine & CStr(FormatNumber(SUMCOSTO, 2))
            
            mRsVer2.MoveNext
        Wend
        mAcum = 0
        While mAcum <= mLonarrMarra
            Set mRep = rpt_OrdenCompraExt
            For k = 0 To mNumColRep - 1
                If mAcum <= mLonarrMarra Then
                    mRep.Sections("Enc_pag").Controls("Cabe" & k + 1).Caption = MARRA(mmarraPosReal(k))
                    mRep.Sections("Detalle").Controls("Texto" & k + 1).DataField = "resultado" & mmarraPosReal(k)
                Else
                    mRep.Sections("Enc_pag").Controls("Cabe" & k + 1).Visible = False
                    mRep.Sections("Detalle").Controls("Texto" & k + 1).Visible = False
                End If
                mAcum = mAcum + 1
            Next
            mRep.Orientation = rptOrientLandscape
            reporteExteDatos pNoDoc, mRep, TablaTr, concepto
            
            For i = 0 To mLonCamposFijos - 1
                mRep.Sections("Enc_pag").Controls("c_campo" & i + 1).Caption = mCamposFijosDes(i)
            Next
            Set mRep.DataSource = mRsVer2
            If pImpresoraoPantalla Then
                mRep.Show vbModal
            Else
                mRep.PrintReport False
            End If
        Wend
    End If
    EnviarDocumento mConexion, pRsCorreo, mEmpresa, pNoDoc
    Exit Sub
erro1:
   ' MsgBox Err.Description
    If mLonarrMarra = -1 Then
        Call Mensaje(True, "No existe el detalle para este documento. Comuniquese con soporte t�cnico.")
    Else
        Call Mensaje(True, Err.Description)
    End If
End Sub

Function MakeRS(ByVal rsSource As ADODB.Recordset) As ADODB.Recordset
Dim RsTemp As ADODB.Recordset, f As ADODB.Field
  Set RsTemp = New ADODB.Recordset
  For Each f In rsSource.Fields
    If f.Type <> adChapter Then
      RsTemp.Fields.Append f.Name, f.Type, f.DefinedSize, f.Attributes And adFldIsNullable
      With RsTemp(f.Name)
        .Precision = f.Precision
        .NumericScale = f.NumericScale
      End With
    End If
  Next f
  Set MakeRS = RsTemp
End Function

Sub OpenAndFillRS(ByVal rsSource As ADODB.Recordset, ByVal rsDest As ADODB.Recordset)
Dim f As ADODB.Field
  If rsSource.State = adStateClosed Then Exit Sub
  If rsSource.EOF And rsSource.BOF Then Exit Sub
  If rsSource.CursorType <> adOpenForwardOnly Then
    If Not rsSource.EOF And Not rsSource.BOF Then
      rsSource.MoveFirst
    End If
  End If
  rsDest.CursorLocation = adUseClient
  rsDest.Open
 
  Do While Not rsSource.EOF
    rsDest.AddNew
    For Each f In rsSource.Fields
      If f.Type <> adChapter Then rsDest(f.Name).value = f.value
    Next f
    rsDest.Update
    rsSource.MoveNext
  Loop
End Sub

Public Sub OrdenCompraExte(pNoDoc As String, pImpresoraoPantalla)
    On Error GoTo erro1
    
    Dim mRs As New ADODB.Recordset
    Dim mRsVer As New ADODB.Recordset
    Dim mCamposFijos
    Dim mNumColRep
    mNumColRep = 12
    mValor = ORDER_PLANTILLA
    SQL = " SELECT     TR_ODC.c_DOCUMENTO, TR_ODC.c_CODARTICULO, TR_ODC.n_CANTIDAD, TR_ODC.n_COSTO, TR_ODC.n_subtotal, TR_ODC.n_impuesto, " & _
                          " TR_ODC.n_total, MA_PRODUCTOS.C_DESCRI, MA_PRODUCTOS.c_marca, MA_PRODUCTOS.c_modelo, MA_PRODUCTOS.C_CODIGO_BASE, " & _
                          " MA_PRODUCTOS.C_DESCRI_BASE, MA_PRODUCTOS.TEXT1, MA_PRODUCTOS.TEXT2, MA_PRODUCTOS.TEXT3, MA_PRODUCTOS.TEXT4, " & _
                          " MA_PRODUCTOS.TEXT5, MA_PRODUCTOS.TEXT6, MA_PRODUCTOS.TEXT7, MA_PRODUCTOS.TEXT8, MA_PRODUCTOS.DATE1, " & _
                          " MA_PRODUCTOS.NUME1, MA_PRODUCTOS.N_CANTIDAD_TMP, MA_PRODUCTOS.C_COD_PLANTILLA, MA_PRODUCTOS.N_PROD_EXT, " & _
                          " MA_PRODUCTOS.N_PRO_EXT,MA_PRODUCTOS.C_DESCRI_BASE as Resultado " & _
    " FROM         TR_ODC INNER JOIN " & _
                          " MA_PRODUCTOS ON TR_ODC.c_CODARTICULO = MA_PRODUCTOS.C_CODIGO " & _
    " WHERE     (TR_ODC.c_DOCUMENTO = '" & pNoDoc & "') " & _
    " ORDER BY " & mValor(1)(1)
    mCamposFijos = Split(mValor(1)(1), ",")
    mCamposFijosDes = Split(mValor(1)(0), ",")
    mLonCamposFijos = UBound(mCamposFijos)
    mCampoVerticalizar = mCamposFijos(mLonCamposFijos)
    
    ReDim Preserve mCamposFijos(mLonCamposFijos - 1)
    'mCamposValores = Array("Resultado", "n_Subtotal", "n_Cantidad", "n_costo")
    mCamposValores = Array("Resultado", "n_Cantidad", "n_costo")
    Set mRs = gClsDatos.BuscarRs(SQL, mConexion)
    mValor = gClsDatos.Verticalizar(mRs, mCamposFijos, CStr(mCampoVerticalizar), 50, mCamposValores)
    Set mRsVer = mValor(0)
    MARRA = mValor(1)
    mmarraOrd = MARRA
    mmarraPosReal = MARRA
    mLonarrMarra = UBound(MARRA)
    gclsbusord_array.Ordenar_valorArray_Quicksort mmarraOrd, 0, mLonarrMarra
    For i = 0 To mLonarrMarra
        mmarraPosReal(i) = gclsbusord_array.buscar_valorArray(MARRA, mmarraOrd(i))
    Next
    
    If Not mRsVer.EOF Then
        mRsVer.MoveFirst
        While Not mRsVer.EOF
            For i = 0 To 50
                mRsVer("Resultado" & i) = mRsVer("n_Cantidad" & i) & vbNewLine & FormatNumber(mRsVer("n_costo" & i), 2)
            Next
            DoEvents
            mRsVer.MoveNext
        Wend
        mAcum = 0
        While mAcum <= mLonarrMarra
            Set mRep = rpt_OrdenCompraExt
            For k = 0 To mNumColRep - 1
                If mAcum <= mLonarrMarra Then
                    mRep.Sections("Enc_pag").Controls("Cabe" & k + 1).Caption = MARRA(mmarraPosReal(k))
                    mRep.Sections("Detalle").Controls("Texto" & k + 1).DataField = "resultado" & mmarraPosReal(k)
                Else
                    mRep.Sections("Enc_pag").Controls("Cabe" & k + 1).Visible = False
                    mRep.Sections("Detalle").Controls("Texto" & k + 1).Visible = False
                End If
                mAcum = mAcum + 1
            Next
            mRep.Orientation = rptOrientLandscape
            OrdenCompraExteDatos pNoDoc, mRep
            For i = 0 To mLonCamposFijos - 1
                mRep.Sections("Enc_pag").Controls("c_campo" & i + 1).Caption = mCamposFijosDes(i)
            Next
            Set mRep.DataSource = mRsVer
            If pImpresoraoPantalla Then
                mRep.Show vbModal
            Else
                mRep.PrintReport
            End If
        Wend
    End If
    Exit Sub
erro1:
    MsgBox Err.Description
End Sub
Private Function generarConsultaDatos(pNoDoc As String, TablaTr As String, concepto As String)


       SQL = ""
       SqlGroup = ""
    
    Select Case UCase(concepto)
       
    Case Is = "ODC"
    
        TablaMa = "MA_ODC"
        
        SQL = "SELECT " & TablaMa & ".c_DOCUMENTO, " & TablaMa & ".d_fecha," _
        & " sum(" & TablaTr & ".n_cantidad) as n_cantidad ," _
        & " CONVERT(NVARCHAR(1000), " & TablaMa & ".c_observacion) AS C_OBSERVACION, " & TablaMa & ".c_CODCOMPRADOR ," _
        & " " & TablaMa & ".n_impuesto," & TablaMa & ".n_subtotal, " _
        & " " & TablaMa & ".n_total, " & TablaMa & ".n_descuento ,  " _
        & " MA_PROVEEDORES.c_razon as c_descripcio,MA_PROVEEDORES.c_direccion, " _
        & " MA_PROVEEDORES.c_telefono,MA_DEPOSITO.c_codlocalidad FROM " & TablaMa & " " _
        & " inner join " & TablaTr & " on " & TablaMa & ".c_documento = " & TablaTr & ".c_documento" _
        & ",MA_PROVEEDORES,MA_DEPOSITO"
        
        sqlWhere = " WHERE " & TablaMa & ".c_CODPROVEEDOR = MA_PROVEEDORES.c_codproveed  AND " & TablaMa & ".C_DESPACHAR = MA_DEPOSITO.C_CODDEPOSITO AND " & TablaMa & ".C_DOCUMENTO = '" & pNoDoc & "'"
        
        SqlGroup = " Group by " & TablaMa & ".C_DOCUMENTO, " & TablaMa & ".D_FECHA, " & TablaMa & ".C_CODCOMPRADOR, " & TablaMa & ".N_IMPUESTO," _
        & TablaMa & ".N_SUBTOTAL, " & TablaMa & ".N_TOTAL, " & TablaMa & ".C_DOCUMENTO, " & TablaMa & ".N_DESCUENTO, MA_PROVEEDORES.c_razon," _
        & " MA_PROVEEDORES.c_direccion, MA_PROVEEDORES.c_telefono, MA_DEPOSITO.c_codlocalidad, CONVERT(NVARCHAR(1000), " & TablaMa & ".c_observacion)"
        
        Debug.Print SQL & sqlWhere & SqlGroup
    
    Case "COM", "DCM", "NDC"
        
        TablaMa = "MA_COMPRAS"
        
        SQL = "SELECT " & TablaMa & ".c_DOCUMENTO, " & TablaMa & ".d_fecha, " & TablaMa & ".FacProveedor, " _
        & " " & TablaMa & ".c_observacion, " & TablaMa & ".c_CODCOMPRADOR ,  " _
        & " " & TablaMa & ".n_impuesto," & TablaMa & ".n_subtotal, " _
        & " " & TablaMa & ".n_total, " & TablaMa & ".n_descuento ,  " _
        & " MA_PROVEEDORES.c_razon as c_descripcio,MA_PROVEEDORES.c_direccion, " _
        & " MA_PROVEEDORES.c_telefono,MA_DEPOSITO.c_codlocalidad FROM " & TablaMa & ",MA_PROVEEDORES,MA_DEPOSITO"
          
        sqlWhere = " WHERE " & TablaMa & ".c_CODPROVEEDOR = MA_PROVEEDORES.c_codproveed  AND " _
        & " " & TablaMa & ".C_CODDEPOSITO = MA_DEPOSITO.C_CODDEPOSITO AND C_DOCUMENTO = '" & pNoDoc & "'" _
        & " AND " & TablaMa & ".C_CONCEPTO = '" & concepto & "'"
        
        Debug.Print SQL & sqlWhere
        
    Case "REC", "AJU", "TRA", "TRS"
            
            TablaMa = "MA_INVENTARIO"
            
            SQL = "SELECT " & TablaMa & ".c_DOCUMENTO, " & TablaMa & ".d_fecha , " & TablaMa & ".c_CODCOMPRADOR , " & TablaMa & ".c_Factura, " _
            & "  sum(" & TablaTr & ".n_cantidad) as n_cantidad ," _
            & "  sum(" & TablaTr & ".n_impuesto)as n_impuesto ," _
            & "  sum(" & TablaTr & ".n_subtotal) as n_subtotal, " _
            & "  sum(" & TablaTr & ".n_total) as n_total ," _
            & " MA_PROVEEDORES.c_razon as c_descripcio,MA_PROVEEDORES.c_direccion, " _
            & " MA_PROVEEDORES.c_telefono,MA_DEPOSITO.c_codlocalidad FROM " & TablaMa & " " _
            & " inner join " & TablaTr & " on " & TablaMa & ".c_documento = " & TablaTr & ".c_documento and " _
            & " " & TablaMa & ".c_concepto = " & TablaTr & ".c_concepto " _
            & "  ,MA_PROVEEDORES,MA_DEPOSITO"
            
            
            sqlWhere = " WHERE " & TablaMa & ".c_CODPROVEEDOR = MA_PROVEEDORES.c_codproveed  AND " _
            & " " & TablaMa & ".c_DEP_ORIG = MA_DEPOSITO.C_CODDEPOSITO AND " & TablaMa & ".C_DOCUMENTO = '" & pNoDoc & "'" _
            & " and " & TablaMa & ".C_CONCEPTO = '" & concepto & "'"
            
            
            SqlGroup = "group by " & TablaMa & ".c_DOCUMENTO,MA_PROVEEDORES.c_razon, " _
            & " MA_PROVEEDORES.c_direccion,MA_PROVEEDORES.c_telefono, " _
            & " MA_DEPOSITO.c_codlocalidad," & TablaMa & ".d_fecha , " & TablaMa & ".c_CODCOMPRADOR, " & TablaMa & ".c_Factura"
            
    Case "INV"
    
            TablaMa = "MA_INV_FISICO"
            
'            sql = "SELECT " & TablaMa & ".c_DOCUMENTO, " & TablaMa & ".d_fecha , " & TablaMa & ".c_CODCOMPRADOR , " _
'            & "   0 as n_impuesto ," _
'            & "   0  as n_subtotal, " _
'            & "   0 as n_total ," _
'            & " MA_SUCURSALES.c_descripcion as c_descripcio,MA_SUCURSALES.c_direccion, " _
'            & " MA_SUCURSALES.c_telefono,MA_DEPOSITO.c_codlocalidad FROM " & TablaMa & " " _
'            & " inner join " & TablaTr & " on " & TablaMa & ".c_documento = " & TablaTr & ".c_documento " _
'            & "  ,MA_SUCURSALES,MA_DEPOSITO"
'
'            sqlWhere = " WHERE " & TablaMa & ".c_DEP_ORIG = MA_SUCURSALES.C_codigo  AND " _
'            & " " & TablaMa & ".c_DEP_ORIG = MA_DEPOSITO.C_CODDEPOSITO AND " & TablaMa & ".C_DOCUMENTO = '" & pNoDoc & "'"
'
            '19695633
            SQL = "SELECT " & TablaMa & ".c_DOCUMENTO, " & TablaMa & ".d_fecha, " & TablaMa & ".c_CODCOMPRADOR," _
            & " sum(" & TablaTr & ".n_cantidad) as n_cantidad ," _
            & " 0 AS n_impuesto," _
            & " 0 AS n_subtotal," _
            & " 0 AS n_total," _
            & " s.c_descripcion as c_descripcio,s.c_direccion, " _
            & " s.c_telefono,d.c_codlocalidad FROM " & TablaMa & " " _
            & " INNER JOIN " & TablaTr & " ON " & TablaMa & ".c_documento = " & TablaTr & ".c_documento " _
            & " ,MA_SUCURSALES s,MA_DEPOSITO d"
            

            sqlWhere = " WHERE LEFT(" & TablaMa & ".c_DEP_ORIG,1) = s.C_codigo  AND " _
            & " " & TablaMa & ".c_DEP_ORIG = d.C_CODDEPOSITO AND " & TablaMa & ".C_DOCUMENTO = '" & pNoDoc & "'"
            
            SqlGroup = " group by " & TablaMa & ".C_DOCUMENTO, " & TablaMa & ".D_FECHA, " & TablaMa & ".C_CODCOMPRADOR, " _
            & " S.C_DESCRIPCION, S.C_TELEFONO, S.C_DIRECCION, D.C_CODLOCALIDAD"
            
            'Debug.Print sql & sqlWhere
            
    End Select
    
      If SQL = "" Then SQL = "SELECT " & TablaMa & ".c_DOCUMENTO, " & TablaMa & ".d_fecha,  " _
       & " " & TablaMa & ".c_observacion, " & TablaMa & ".c_CODCOMPRADOR ,  " _
       & " " & TablaMa & ".n_impuesto," & TablaMa & ".n_subtotal, " _
       & " " & TablaMa & ".n_total, " & TablaMa & ".n_descuento ,  " _
       & " MA_PROVEEDORES.c_razon as c_descripcio,MA_PROVEEDORES.c_direccion, " _
       & " MA_PROVEEDORES.c_telefono,MA_DEPOSITO.c_codlocalidad FROM " & TablaMa & ",MA_PROVEEDORES,MA_DEPOSITO"
    
    
    generarConsultaDatos = SQL & sqlWhere & SqlGroup
    Debug.Print generarConsultaDatos
End Function

Private Sub reporteExteDatos(pNoDoc As String, pReport, TablaTr As String, concepto As String)
   Dim RsCompras As ADODB.Recordset
   Dim RsSucursales As ADODB.Recordset
   Dim docCaption As String
   Dim CargarMontos As Boolean
    
    
    CargarMontos = False
    
    Select Case UCase(concepto)
     Case Is = "ODC"
        
        docCaption = "ORDEN DE COMPRA No."
        CargarMontos = True
     
     Case "COM", "DCM", "NDC"
        
        CargarMontos = True
        
        If UCase(concepto) = "COM" Then docCaption = "COMPRA No."
        If UCase(concepto) = "DCM" Then docCaption = "DEVOLUCION DE COMPRA No."
        If UCase(concepto) = "NDC" Then docCaption = "NOTA DE DEVOLUCION DE COMPRA No."
        
    Case "REC", "AJU", "INV", "TRA", "TRS"
        
        CargarMontos = False
            If UCase(concepto) = "REC" Then docCaption = "RECEPCION No."
            If UCase(concepto) = "AJU" Then docCaption = "AJUSTE No."
            If UCase(concepto) = "INV" Then
                    docCaption = "INVENTARIO FISICO No."
                    pReport.Sections("ENC_PAG").Controls("Etiqueta2").Caption = "Localidad"
                    pReport.Sections("ENC_PAG").Controls("L�nea15").Visible = False
                    pReport.Sections("ENC_PAG").Controls("Etiqueta3").Visible = False
                    pReport.Sections("ENC_PAG").Controls("L�nea18").Visible = False
                    pReport.Sections("ENC_PAG").Controls("L�nea17").Visible = False
                    pReport.Sections("ENC_PAG").Controls("LBL_DESTINO").Visible = False
                    pReport.Sections("ENC_PAG").Controls("LBL_DIRECCION_DESTINO").Visible = False
                    pReport.Sections("ENC_PAG").Controls("LBL_TELEFONO_DESTINO").Visible = False
                    pReport.Sections("PIE_PAG").Controls("LBL_IMPUESTO").Visible = False
                    pReport.Sections("PIE_PAG").Controls("LBL_TOTAL").Visible = False
                    pReport.Sections("PIE_PAG").Controls("LBL_SUBTOTAL").Visible = False
                    pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Visible = False
                    pReport.Sections("PIE_PAG").Controls("Etiqueta14").Visible = False
                    pReport.Sections("PIE_PAG").Controls("Etiqueta13").Visible = False
                    pReport.Sections("PIE_PAG").Controls("Etiqueta16").Visible = False
                    pReport.Sections("PIE_PAG").Controls("Etiqueta15").Visible = False
            End If
            If UCase(concepto) = "TRA" Then docCaption = "TRASLADO No."
            If UCase(concepto) = "TRS" Then docCaption = "TRANSFERENCIA No."
                 
        
    End Select
    
    consulta = generarConsultaDatos(pNoDoc, TablaTr, concepto)
   
    Call Apertura_RecordsetC(RsCompras)
    
   
    RsCompras.Open consulta, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText

    Sucursal = RsCompras!c_CodLocalidad
    
    REPO_CABE pReport, ""
    
'    nombre_usuario = buscar_usuario(rsCompras!c_codcomprador)
'    pReport.Sections("ENC_PAG").Controls("LBL_FECHA_FAC").Visible = True
'    pReport.Sections("ENC_PAG").Controls("LBL_FACTURA").Visible = True
'    pReport.Sections("ENC_PAG").Controls("FACTURA").Visible = True
'    pReport.Sections("ENC_PAG").Controls("FECHA").Visible = True
'    pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Visible = True
'
'    pReport.Sections("ENC_PAG").Controls("QUE_DOCUMENTO").Caption = docCaption
'    pReport.Sections("ENC_PAG").Controls("LBL_DOCUMENTO").Caption = Trim(rsCompras!C_documento)
'    pReport.Sections("ENC_PAG").Controls("LBL_FECHA").Caption = rsCompras!d_fecha
'
'    pReport.Sections("ENC_PAG").Controls("LBL_PROVEEDOR").Caption = rsCompras!c_descripcio
'    pReport.Sections("ENC_PAG").Controls("LBL_DIRECCION_ORIGEN").Caption = rsCompras!c_direccion
'    pReport.Sections("ENC_PAG").Controls("LBL_TELEFONO_ORIGEN").Caption = "TELEFONOS:" & rsCompras!c_telefono
'    Sucursal = rsCompras!c_codlocalidad
'
'   '
'    pReport.Sections("PIE_PAG").Controls("usuario").Caption = nombre_usuario
'
'    pReport.Sections("PIE_PAG").Controls("LBL_IMPUESTO").Caption = FormatNumber(rsCompras!n_impuesto, 2)
'    pReport.Sections("PIE_PAG").Controls("LBL_TOTAL").Caption = FormatNumber(rsCompras!n_total, 2)
'    pReport.Sections("PIE_PAG").Controls("LBL_SUBTOTAL").Caption = FormatNumber(rsCompras!n_subtotal, 2)

    '19695633
    If Not RsCompras.EOF Then
        nombre_usuario = buscar_usuario(mConexion, RsCompras!c_codcomprador)
        pReport.Sections("ENC_PAG").Controls("LBL_FECHA_FAC").Visible = True
        pReport.Sections("ENC_PAG").Controls("FECHA").Visible = True
        pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Visible = True
         
        pReport.Sections("ENC_PAG").Controls("QUE_DOCUMENTO").Caption = docCaption
        pReport.Sections("ENC_PAG").Controls("LBL_DOCUMENTO").Caption = Trim(RsCompras!c_Documento)
        pReport.Sections("ENC_PAG").Controls("LBL_FECHA").Caption = RsCompras!d_Fecha
        
        pReport.Sections("ENC_PAG").Controls("LBL_PROVEEDOR").Caption = RsCompras!c_Descripcio
        pReport.Sections("ENC_PAG").Controls("LBL_DIRECCION_ORIGEN").Caption = RsCompras!c_Direccion
        pReport.Sections("ENC_PAG").Controls("LBL_TELEFONO_ORIGEN").Caption = "TELEFONOS:" & RsCompras!c_Telefono
        Sucursal = RsCompras!c_CodLocalidad
   '
        pReport.Sections("PIE_PAG").Controls("usuario").Caption = nombre_usuario
        
        
        Select Case UCase(concepto)
        
        Case Is = "COM"
        
        pReport.Sections("ENC_PAG").Controls("FACTURA").Visible = True
        pReport.Sections("ENC_PAG").Controls("LBL_FACTURA").Visible = True
        pReport.Sections("ENC_PAG").Controls("LBL_FACTURA").Caption = RsCompras!FACPROVEEDOR
        
        Case Is = "REC"
        
        pReport.Sections("PIE_PAG").Controls("ETIQUETA_UNIDADES").Visible = True
        pReport.Sections("PIE_PAG").Controls("LBL_UNIDADES").Visible = True
        pReport.Sections("PIE_PAG").Controls("LBL_UNIDADES").Caption = FormatNumber(RsCompras!n_Cantidad, 2)
        
        pReport.Sections("ENC_PAG").Controls("FACTURA").Visible = True
        pReport.Sections("ENC_PAG").Controls("LBL_FACTURA").Visible = True
        pReport.Sections("ENC_PAG").Controls("LBL_FACTURA").Caption = RsCompras!c_Factura
        
        Case Is = "ODC"
        
        pReport.Sections("PIE_PAG").Controls("ETIQUETA_UNIDADES").Visible = True
        pReport.Sections("PIE_PAG").Controls("LBL_UNIDADES").Visible = True
        pReport.Sections("PIE_PAG").Controls("LBL_UNIDADES").Caption = FormatNumber(RsCompras!n_Cantidad, 2)
        
        Case Is = "INV"
        
        pReport.Sections("PIE_PAG").Controls("ETIQUETA_UNIDADES").Visible = True
        pReport.Sections("PIE_PAG").Controls("LBL_UNIDADES").Visible = True
        pReport.Sections("PIE_PAG").Controls("LBL_UNIDADES").Caption = FormatNumber(RsCompras!n_Cantidad, 2)
        
        End Select
        
        pReport.Sections("PIE_PAG").Controls("LBL_IMPUESTO").Caption = FormatNumber(RsCompras!n_Impuesto, 2)
        pReport.Sections("PIE_PAG").Controls("LBL_TOTAL").Caption = FormatNumber(RsCompras!n_Total, 2)
        pReport.Sections("PIE_PAG").Controls("LBL_SUBTOTAL").Caption = FormatNumber(RsCompras!n_Subtotal, 2)
   End If
   
   If CargarMontos Then
       pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Caption = FormatNumber(RsCompras!n_Descuento, 2)
     '  pReport.Sections("PIE_PAG").Controls("fecha_recepcion").Caption = "Fecha de Recepci�n: " & Format(rsCompras!d_fecha_recepcion, "short date")
       pReport.Sections("PIE_INF").Controls("LBL_OBSERVACION").Caption = "OBSERVACI�N: " & IIf(IsNull(RsCompras!c_Observacion), "", RsCompras!c_Observacion)
  Else
        'cambiar esto por los nombres de las etiquetas para que salgan acorde al reporte
        '    pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Caption = FormatNumber(0, 2)
             pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Visible = False
            pReport.Sections("PIE_PAG").Controls("Etiqueta14").Visible = False
        '    pReport.Sections("PIE_PAG").Controls("LBL_TOTAL").Caption = FormatNumber(0, 2)
        '    pReport.Sections("PIE_PAG").Controls("LBL_SUBTOTAL").Caption = FormatNumber(0, 2)
  End If
    Call Apertura_RecordsetC(RsSucursales)
    RsSucursales.Open "SELECT * FROM MA_SUCURSALES WHERE C_CODIGO = '" & Sucursal & "'", mConexion, adOpenDynamic, adLockReadOnly, adCmdText
    
    pReport.Sections("ENC_PAG").Controls("LBL_DESTINO").Caption = RsSucursales!c_Descripcion
    pReport.Sections("ENC_PAG").Controls("LBL_DIRECCION_DESTINO").Caption = RsSucursales!c_Direccion
    pReport.Sections("ENC_PAG").Controls("LBL_TELEFONO_DESTINO").Caption = "TELEFONOS:" & RsSucursales!c_Telefono



End Sub
Private Sub OrdenCompraExteDatos(pNoDoc As String, pReport)
    Dim RsCompras As ADODB.Recordset
    Dim RsSucursales As ADODB.Recordset
    
    Call Apertura_RecordsetC(RsCompras)
    REPO_CABE pReport, ""
    RsCompras.Open "SELECT MA_ODC.*,MA_PROVEEDORES.c_razon as c_descripcio,MA_PROVEEDORES.c_direccion,MA_PROVEEDORES.c_telefono,MA_DEPOSITO.c_codlocalidad FROM MA_ODC,MA_PROVEEDORES,MA_DEPOSITO WHERE MA_ODC.c_CODPROVEEDOR = MA_PROVEEDORES.c_codproveed  AND MA_ODC.C_DESPACHAR = MA_DEPOSITO.C_CODDEPOSITO AND C_DOCUMENTO = '" & pNoDoc & "'", mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    nombre_usuario = buscar_usuario(mConexion, RsCompras!c_codcomprador)
    pReport.Sections("ENC_PAG").Controls("LBL_FECHA_FAC").Visible = True
    pReport.Sections("ENC_PAG").Controls("LBL_FACTURA").Visible = True
    pReport.Sections("ENC_PAG").Controls("FACTURA").Visible = True
    pReport.Sections("ENC_PAG").Controls("FECHA").Visible = True
    pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Visible = True
     
    pReport.Sections("ENC_PAG").Controls("QUE_DOCUMENTO").Caption = "ORDEN DE COMPRA No."
    pReport.Sections("PIE_PAG").Controls("fecha_recepcion").Caption = "Fecha de Recepci�n: " & Format(RsCompras!d_Fecha_Recepcion, "short date")
    pReport.Sections("ENC_PAG").Controls("LBL_DOCUMENTO").Caption = Trim(RsCompras!c_Documento)
    pReport.Sections("ENC_PAG").Controls("LBL_FECHA").Caption = RsCompras!d_Fecha
    
    pReport.Sections("ENC_PAG").Controls("LBL_PROVEEDOR").Caption = RsCompras!c_Descripcio
    pReport.Sections("ENC_PAG").Controls("LBL_DIRECCION_ORIGEN").Caption = RsCompras!c_Direccion
    pReport.Sections("ENC_PAG").Controls("LBL_TELEFONO_ORIGEN").Caption = "TELEFONOS:" & RsCompras!c_Telefono
    Sucursal = RsCompras!c_CodLocalidad
    
    pReport.Sections("PIE_INF").Controls("LBL_OBSERVACION").Caption = "OBSERVACI�N: " & IIf(IsNull(RsCompras!c_Observacion), "", RsCompras!c_Observacion)
    pReport.Sections("PIE_PAG").Controls("usuario").Caption = nombre_usuario
    pReport.Sections("PIE_PAG").Controls("LBL_SUBTOTAL").Caption = FormatNumber(RsCompras!n_Subtotal, 2)
    pReport.Sections("PIE_PAG").Controls("LBL_DESCUENTO").Caption = FormatNumber(RsCompras!n_Descuento, 2)
    pReport.Sections("PIE_PAG").Controls("LBL_IMPUESTO").Caption = FormatNumber(RsCompras!n_Impuesto, 2)
    pReport.Sections("PIE_PAG").Controls("LBL_TOTAL").Caption = FormatNumber(RsCompras!n_Total, 2)
    Call Apertura_RecordsetC(RsSucursales)
    RsSucursales.Open "SELECT * FROM MA_SUCURSALES WHERE C_CODIGO = '" & Sucursal & "'", mConexion, adOpenDynamic, adLockReadOnly, adCmdText
    
    pReport.Sections("ENC_PAG").Controls("LBL_DESTINO").Caption = RsSucursales!c_Descripcion
    pReport.Sections("ENC_PAG").Controls("LBL_DIRECCION_DESTINO").Caption = RsSucursales!c_Direccion
    pReport.Sections("ENC_PAG").Controls("LBL_TELEFONO_DESTINO").Caption = "TELEFONOS:" & RsSucursales!c_Telefono


End Sub

Private Function BuscarNombreEmpresa() As String
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "Select nom_org from estruc_sis "
    mRs.Open mSQL, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    If Not mRs.EOF Then
        BuscarNombreEmpresa = mRs!Nom_Org
    End If
    
End Function

