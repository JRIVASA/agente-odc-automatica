VERSION 5.00
Begin VB.Form frmDebug 
   Caption         =   "Odc Auto"
   ClientHeight    =   3960
   ClientLeft      =   225
   ClientTop       =   855
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3960
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.Menu menu 
      Caption         =   "Menu"
      Begin VB.Menu config 
         Caption         =   "Configurar ODC"
      End
      Begin VB.Menu agente 
         Caption         =   "Agente ODC"
      End
   End
End
Attribute VB_Name = "frmDebug"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub agente_Click()
    
    Dim mCls As New clsOdcAuto
    
    mCls.IniciarAgenteOdc AppDatos.Conexion
    
End Sub

Private Sub config_Click()
    
    frmProveedor.Show vbModal
    Set frmProveedor = Nothing
    
End Sub
