VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsOdcAutoConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Enum eOdcAutoTipoProg
    eOdcAutoDiaria
    eOdcAutoSemanal
    eOdcAutoMensual
End Enum

Enum eTipoCosto
    etcActual
    etcAnterior
    etcPromedio
    etcReposicion
End Enum

Private mConexion                                       As ADODB.Connection
Private mClsConfig                                      As clsOdcAutoConfigProp
Private mvarUsuario                                     As String

Property Get ClsConfigProp() As clsOdcAutoConfigProp
    Set ClsConfigProp = mClsConfig
End Property

Public Sub IntefazConfiguracion(pCn As ADODB.Connection, _
pProveedor As String, pNomProveedor As String, _
pSucursal As String, pNomSucursal As String, _
pUsuario As String)
    
    Set mConexion = pCn
    
    mvarUsuario = pUsuario
    mvarProveedor = pProveedor
    
    IniciarClaseConfig pProveedor, pNomProveedor, pSucursal, pNomSucursal
    
    Set frmConfig.fCls = Me
    
    frmConfig.Show vbModal
    
    Set frmConfig = Nothing
    
End Sub

Private Sub IniciarClaseConfig(pProveedor As String, pNomProveedor As String, _
pSucursal As String, pNomSucursal As String)
    
    If BuscarConfiguracionProveedor(mConexion, pProveedor, pSucursal, pNomProveedor) Then
        mClsConfig.NomProveedor = pNomProveedor
        mClsConfig.Sucursal = pSucursal
    Else
        Set mClsConfig = New clsOdcAutoConfigProp
        mClsConfig.Sucursal = pSucursal
        mClsConfig.CodProveedor = pProveedor
        mClsConfig.NomProveedor = pNomProveedor
        mClsConfig.NomSucursal = pNomSucursal
        BuscarDepositosSucursal mClsConfig
    End If
    
End Sub

Private Sub AsignarPropiedadesCls(pRs As ADODB.Recordset, _
pProveedor As String, pNomProveedor As String, _
pSucursal As String, pNomSucursal As String)
    
    Set mClsConfig = New clsOdcAutoConfigProp
    
    With mClsConfig
        
        .CadaNIntervalo = pRs!n_Cada
        .CodProveedor = pProveedor
        .NomProveedor = pNomProveedor
        .NomSucursal = pNomSucursal
        .Sucursal = pSucursal
        .Dia = pRs!n_Dia
        .DiasAnalisis = pRs!n_DiasAnalisis
        .DiasDespachar = pRs!n_DiasDespacho
        .DiasReposicion = pRs!n_DiasReposicion
        .DiasSel = pRs!n_DiasSel
        .PromediarVentas = pRs!b_Promediar
        .ConsideraDescargos = pRs!b_Descargos
        
        If pRs!n_TipoCosto > 0 And pRs!n_TipoCosto <= pRs!n_DiasReposicion Then
            .TipoCosto = pRs!n_TipoCosto
        End If
        
        If pRs!n_Tipo > 0 And pRs!n_Tipo <= eOdcAutoMensual Then
            .TipoProg = pRs!n_Tipo
        End If
        
    End With
    
    BuscarDepositosSucursal mClsConfig
    
End Sub

Private Sub BuscarDepositosSucursal(pClsConfig As clsOdcAutoConfigProp)
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    Dim mDep As clsOdcAutoDeposito
    
    pClsConfig.IniciarDepositos
    
    mSQL = "Select d.*, coalesce(odc.id, 0) as seleccion, " & _
    "coalesce(odc.b_predeterminado, x0) as predeterminado " & _
    "from MA_DEPOSITO d " & _
    "left join MA_ODC_PROGRAMADAS_DEP odc " & _
    "on odc.c_coddeposito = d.c_coddeposito " & _
    "and odc.c_proveedor = '" & pClsConfig.CodProveedor & "' " & _
    "where d.c_codlocalidad = '" & pClsConfig.Sucursal & "' "
    
    'Debug.Print mSql
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, mConexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Do While Not mRs.EOF
        
        Set mDep = New clsOdcAutoDeposito
        
        pClsConfig.Depositos.add mDep.AddDepositoRs(mRs)
        
        mRs.MoveNext
        
    Loop
    
    mRs.Close
    
End Sub

Public Function Grabar() As Boolean
    
    Dim mInicio As Boolean
    
    On Error GoTo Errores
    
    mConexion.BeginTrans
    
    mInicio = True
    
    GrabarConfiguracion mConexion
    GrabarConfiguracionDepositos mConexion
    
    mConexion.CommitTrans
    mInicio = False
    
    Grabar = True
    
    Exit Function
    
Errores:
    
    If mInicio Then
        mConexion.RollbackTrans
        mInicio = False
    End If
    
    MsgBox Err.Description, vbCritical
    
    Err.Clear
    
End Function

Private Sub GrabarConfiguracion(pCn As ADODB.Connection)
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    
    mSQL = "Select * from MA_ODC_PROGRAMADAS " & _
    "where c_sucursal = '" & mClsConfig.Sucursal & "' " & _
    "and c_proveedor = '" & mClsConfig.CodProveedor & "' "
    
    Set mRs = New ADODB.Recordset
    
    mRs.Open mSQL, pCn, adOpenKeyset, adLockPessimistic, adCmdText
    
    If mRs.EOF Then mRs.AddNew
    
    With mClsConfig
        
        mRs!c_proveedor = .CodProveedor
        mRs!c_Sucursal = .Sucursal
        mRs!n_Cada = .CadaNIntervalo
        mRs!n_Dia = .Dia
        mRs!n_DiasAnalisis = .DiasAnalisis
        mRs!n_DiasDespacho = .DiasDespachar
        mRs!n_DiasReposicion = .DiasReposicion
        mRs!n_DiasSel = .DiasSel
        mRs!n_TipoCosto = .TipoCosto
        mRs!n_Tipo = .TipoProg
        mRs!b_Promediar = .PromediarVentas
        mRs!b_Descargos = .ConsideraDescargos
        mRs!c_usuario = mvarUsuario
        
        mRs.Update
        
    End With
    
End Sub

Private Sub GrabarConfiguracionDepositos(pCn As ADODB.Connection)
    
    Dim mDep As clsOdcAutoDeposito
    Dim mSQL As String
    
    EliminarDatosTblConfig pCn
    
    For Each mDep In mClsConfig.Depositos
        
        If mDep.Seleccionado Then
            
            mSQL = "Insert into MA_ODC_PROGRAMADAS_DEP " & _
            "(c_proveedor, c_sucursal, c_coddeposito, b_predeterminado) " & _
            "values ('" & mClsConfig.CodProveedor & "', '" & mClsConfig.Sucursal & "', " & _
            "'" & mDep.Codigo & "', " & IIf(mDep.Predeterminado, 1, 0) & ") "
            
            pCn.Execute mSQL
            
        End If
        
    Next
    
End Sub

Private Sub EliminarDatosTblConfig(pCn As ADODB.Connection, Optional pEliminar As Boolean = False)
    
    mSQL = "Delete from MA_ODC_PROGRAMADAS_DEP " & _
    "where c_proveedor = '" & mClsConfig.CodProveedor & "' " & _
    "and c_sucursal = '" & mClsConfig.Sucursal & "' "
    
    pCn.Execute mSQL
    
    If pEliminar Then
        
        mSQL = "Delete from MA_ODC_PROGRAMADAS " & _
        "where c_proveedor = '" & mClsConfig.CodProveedor & "' " & _
        "and c_sucursal = '" & mClsConfig.Sucursal & "' "
        
        pCn.Execute mSQL
        
    End If
    
End Sub

Public Function EliminarConfiguracion(pCn As ADODB.Connection, _
pProveedor As String, pSucursal As String) As Boolean
    
    On Error GoTo Errores
    
    Set mClsConfig = New clsOdcAutoConfigProp
    
    mClsConfig.CodProveedor = pProveedor
    mClsConfig.Sucursal = pSucursal
    
    pCn.BeginTrans
    
    EliminarDatosTblConfig pCn, True
    
    pCn.CommitTrans
    
    EliminarConfiguracion = True
    
    Exit Function
    
Errores:
    
    If mInicio Then mConexion.RollbackTrans
    
    MsgBox Err.Description, vbCritical
    
    Err.Clear
    
End Function

'********************************* metodos formulario proveedores ****************************************
Public Function BuscarDescripcionProgramacion(pCn As ADODB.Connection, pCodigo As String, pProveedor As String) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    Dim mClsConfig As New clsOdcAutoConfigProp
    
    mSQL = "Select * from MA_ODC_PROGRAMADAS " & _
    "where c_proveedor = '" & pProveedor & "' " & _
    "and c_sucursal = '" & pCodigo & "' "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        mDescri = "Sucede " & mClsConfig.ObtenerTipoProg(mRs!n_Tipo) & "mente,"
        
        If mRs!n_Tipo = eOdcAutoDiaria Then
            BuscarDescripcionProgramacion = mDescri & " cada " & mRs!n_Cada & " Dia(s)"
        ElseIf mRs!n_Tipo = eOdcAutoMensual Then
            BuscarDescripcionProgramacion = mDescri & " cada " & mRs!n_Cada & " Mese(s)"
        Else
            'eOdcAutoSemanal
            BuscarDescripcionProgramacion = mDescri & " cada " & mRs!n_Cada & " Semana(s)"
        End If
        
    Else
        BuscarDescripcionProgramacion = "No posee Programacion"
    End If
    
    mRs.Close
    
End Function

'********************************* Metodos para Agente ODC *********************************************************

Public Function BuscarConfiguracionProveedor(pCn As ADODB.Connection, pProveedor As String, _
pSucursal As String, pNomProveedor As String) As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    Set mConexion = pCn
    Set mRs = New ADODB.Recordset
    
    mSQL = "Select * from MA_ODC_PROGRAMADAS " & _
    "where c_sucursal = '" & pSucursal & "' " & _
    "and c_proveedor = '" & pProveedor & "' "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        
        Call AsignarPropiedadesCls(mRs, pProveedor, pNomProveedor, pSucursal, "")
        
        BuscarConfiguracionProveedor = True
        
    End If
    
    mRs.Close
    
    Exit Function
    
Errores:
    
    'MsgBox Err.Description, vbCritical
    EscribirError Err, "Buscando Configuracion. BuscarConfiguracionProveedor()"
    Err.Clear
    
End Function

