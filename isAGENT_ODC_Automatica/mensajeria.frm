VERSION 5.00
Begin VB.Form frm_MENSAJERIA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Centro de Información Stellar"
   ClientHeight    =   2310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6840
   Icon            =   "mensajeria.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2310
   ScaleWidth      =   6840
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cancelar 
      Caption         =   "&Cancelar"
      CausesValidation=   0   'False
      Height          =   825
      Left            =   5685
      Picture         =   "mensajeria.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1425
      Width           =   1095
   End
   Begin VB.CommandButton aceptar 
      Caption         =   "&Aceptar"
      CausesValidation=   0   'False
      Default         =   -1  'True
      Height          =   825
      Left            =   4515
      Picture         =   "mensajeria.frx":6F54
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1425
      Width           =   1095
   End
   Begin VB.TextBox mensaje 
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1320
      Left            =   750
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   60
      Width           =   6045
   End
   Begin VB.Label lbl_Progress 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   180
      TabIndex        =   3
      Top             =   960
      Width           =   75
   End
   Begin VB.Image icono 
      Height          =   570
      Left            =   90
      Picture         =   "mensajeria.frx":7C1E
      Stretch         =   -1  'True
      Top             =   60
      Width           =   585
   End
End
Attribute VB_Name = "frm_MENSAJERIA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub aceptar_Click()
    Tecla_pulsada = False
        Retorno = True
    Unload Me
End Sub

Private Sub cancelar_Click()
    Retorno = False
    Unload Me
End Sub

Private Sub Form_Load()
    Screen.MousePointer = vbDefault
    If uno = True Then
        cancelar.Enabled = False
'        cancelar.Visible = False
'        frm_MENSAJERIA.Width = 7605
    Else
        cancelar.Enabled = True
'        cancelar.Visible = True
'        frm_MENSAJERIA.Width = 8730
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frm_MENSAJERIA = Nothing
End Sub
