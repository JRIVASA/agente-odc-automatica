VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsOdcAutoConfigProp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarSucursal                                                As String
Private mvarNomSucursal                                             As String
Private mvarCodProveedor                                            As String
Private mvarNomProveedor                                            As String
Private mvarTipoProg                                                As eOdcAutoTipoProg
Private mvarTipoCosto                                               As eTipoCosto
Private mvarNCada                                                   As Integer
Private mvarDia                                                     As Integer
Private mvarDiasSel                                                 As Integer
Private mvarDiasReposicion                                          As Integer
Private mvarDiasAnalisis                                            As Integer
Private mvarDiasDespachar                                           As Integer
Private mvarPromediarVentas                                         As Boolean
Private mvarConsideraDescargos                                      As Boolean
Private mDepositos                                                  As Collection
Const nDiasSemana                                                   As Integer = 7

Property Get Sucursal() As String
    Sucursal = mvarSucursal
End Property

Property Get NomSucursal() As String
    NomSucursal = mvarNomSucursal
End Property

Property Get CodProveedor() As String
    CodProveedor = mvarCodProveedor
End Property

Property Get NomProveedor() As String
    NomProveedor = mvarNomProveedor
End Property

Property Get TipoProg() As eOdcAutoTipoProg
    TipoProg = mvarTipoProg
End Property

Property Get TipoCosto() As eTipoCosto
    TipoCosto = mvarTipoCosto
End Property

Property Get CadaNIntervalo() As Integer
    CadaNIntervalo = mvarNCada
End Property

Property Get Dia() As Integer
    Dia = mvarDia
End Property

Property Get DiasSel() As Integer
    DiasSel = mvarDiasSel
End Property

Property Get DiasReposicion() As Integer
    DiasReposicion = mvarDiasReposicion
End Property

Property Get DiasAnalisis() As Integer
    DiasAnalisis = mvarDiasAnalisis
End Property

Property Get DiasDespachar() As Integer
    DiasDespachar = mvarDiasDespachar
End Property

Property Get PromediarVentas() As Boolean
    PromediarVentas = mvarPromediarVentas
End Property

Property Get Depositos() As Collection
    Set Depositos = mDepositos
End Property

Property Get DescripcionTipoProg() As String
    DescripcionTipoProg = ObtenerTipoProg(CInt(mvarTipoProg))
End Property

Property Get DescripcionTipoCosto() As String
    DescripcionTipoCosto = ObtenerTipoCosto(CInt(mvarTipoCosto))
End Property

Property Get ConsideraDescargos() As Boolean
    ConsideraDescargos = mvarConsideraDescargos
End Property

Property Get ArrayDiasSeleccionados()
    
    Dim mArrayDias()
    Dim mDiasSelBin As String
    
    mDiasSelBin = ConvertirBinario(mvarDiasSel)
    
    For i = 1 To Len(mDiasSelBin)
        ReDim Preserve mArrayDias(i - 1)
        mArrayDias(i - 1) = Mid(mDiasSelBin, i, 1)
    Next i
    
    ArrayDiasSeleccionados = mArrayDias
    
End Property

Property Let Sucursal(pValor As String)
    mvarSucursal = pValor
End Property

Property Let NomSucursal(pValor As String)
    mvarNomSucursal = pValor
End Property

Property Let CodProveedor(pValor As String)
    mvarCodProveedor = pValor
End Property

Property Let NomProveedor(pValor As String)
    mvarNomProveedor = pValor
End Property

Property Let TipoProg(pValor As eOdcAutoTipoProg)
    mvarTipoProg = pValor
End Property

Property Let TipoCosto(pValor As eTipoCosto)
    mvarTipoCosto = pValor
End Property

Property Let CadaNIntervalo(pValor As Integer)
    mvarNCada = pValor
End Property

Property Let Dia(pValor As Integer)
    mvarDia = pValor
End Property

Property Let DiasSel(pValor As Integer)
    mvarDiasSel = pValor
End Property

Property Let DiasReposicion(pValor As Integer)
    mvarDiasReposicion = pValor
End Property

Property Let DiasAnalisis(pValor As Integer)
    mvarDiasAnalisis = pValor
End Property

Property Let DiasDespachar(pValor As Integer)
    mvarDiasDespachar = pValor
End Property

Property Let PromediarVentas(pValor As Boolean)
    mvarPromediarVentas = pValor
End Property

Property Let ConsideraDescargos(pValor As Boolean)
    mvarConsideraDescargos = pValor
End Property

Function ObtenerTipoProg(pTipoprod As Integer) As String
    
    Select Case pTipoprod
        Case eOdcAutoTipoProg.eOdcAutoDiaria
            ObtenerTipoProg = "Diaria"
        Case eOdcAutoTipoProg.eOdcAutoSemanal
            ObtenerTipoProg = "Semanal"
        Case Else
            ObtenerTipoProg = "Mensual"
    End Select
    
End Function

Function ObtenerTipoCosto(pTipoCosto As Integer) As String
    
    Select Case pTipoCosto
        Case eTipoCosto.etcActual
            ObtenerTipoCosto = "Actual"
        Case eTipoCosto.etcAnterior
            ObtenerTipoCosto = "Anterior"
        Case eTipoCosto.etcPromedio
            ObtenerTipoCosto = "Promedio"
        Case Else
            ObtenerTipoCosto = "Reposicion"
    End Select
    
End Function

Private Function ConvertirBinario(pNumero As Integer) As String
    
    Dim mResultado As String
    Dim nResul As Single
    
    For i = nDiasSemana - 1 To 0 Step -1
        ConvertirBinario = IIf(pNumero And (2 ^ i), "1", "0") & ConvertirBinario
    Next i
    
End Function

Public Sub IniciarDepositos()
    Set mDepositos = New Collection
End Sub
