VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmProveedor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ficha Proveedores"
   ClientHeight    =   5745
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8310
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5745
   ScaleWidth      =   8310
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   5535
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   8175
      Begin MSComctlLib.ListView lvsucursales 
         Height          =   5055
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   7815
         _ExtentX        =   13785
         _ExtentY        =   8916
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Sucursal"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Descripcion"
            Object.Width           =   7056
         EndProperty
      End
   End
   Begin VB.Menu mnu 
      Caption         =   "menu"
      Visible         =   0   'False
      Begin VB.Menu add 
         Caption         =   "Agregar Configuracion"
      End
      Begin VB.Menu mod 
         Caption         =   "Modificar Configuracion"
      End
      Begin VB.Menu del 
         Caption         =   "Eliminar Configuracion"
      End
   End
End
Attribute VB_Name = "frmProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mCodProveedor As String

Private Sub add_Click()
    
    Dim mCls As New clsOdcAutoConfig
    
    mCls.IntefazConfiguracion AppDatos.Conexion, mCodProveedor, "Hugo", lvsucursales.SelectedItem.Tag, lvsucursales.SelectedItem.Text, "99999999"
    lvsucursales.SelectedItem.SubItems(1) = mCls.BuscarDescripcionProgramacion(AppDatos.Conexion, lvsucursales.SelectedItem.Tag, mCodProveedor)
    
End Sub

Private Sub lvsucursales_DblClick()
    add_Click
End Sub

Private Sub mod_Click()
    Dim mCls As New clsOdcAutoConfig
    
    mCls.IntefazConfiguracion AppDatos.Conexion, mCodProveedor, "Hugo", lvsucursales.SelectedItem.Tag, lvsucursales.SelectedItem.Text, "99999999"
    lvsucursales.SelectedItem.SubItems(1) = mCls.BuscarDescripcionProgramacion(AppDatos.Conexion, lvsucursales.SelectedItem.Tag, mCodProveedor)
End Sub

Private Sub del_Click()
    Dim mCls As New clsOdcAutoConfig
    
    If MsgBox("Esta Seguro de Eliminar la Configuracion de la Sucursal " & lvsucursales.SelectedItem.Text, vbYesNo) = vbYes Then
        If mCls.EliminarConfiguracion(AppDatos.Conexion, mCodProveedor, lvsucursales.SelectedItem.Tag) Then
           lvsucursales.SelectedItem.SubItems(1) = "No posee Programacion"
        End If
    End If
End Sub

Private Sub Form_Load()
    mCodProveedor = "000000019"
   
    LlenarListView
End Sub

Private Sub LlenarListView()
    
    Dim mRs As ADODB.Recordset
    Dim mCls As New clsOdcAutoConfig
    Dim mItm As ListItem
    Dim mSQl As String
    
    mSQl = "Select * from ma_sucursales order by c_codigo "
    Set mRs = New ADODB.Recordset
    mRs.Open mSQl, AppDatos.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Do While Not mRs.EOF
        Set mItm = Me.lvsucursales.ListItems.add(, , mRs!c_descripcion)
        mItm.SubItems(1) = mCls.BuscarDescripcionProgramacion(AppDatos.Conexion, mRs!c_codigo, mCodProveedor)
        mItm.Tag = mRs!c_codigo
        mRs.MoveNext
    Loop
    
End Sub

Private Sub lvsucursales_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    Select Case Button
        Case vbRightButton
            If Not lvsucursales.SelectedItem Is Nothing Then
                Me.add.Enabled = InStr(1, lvsucursales.SelectedItem.SubItems(1), "No", vbTextCompare) > 0
                Me.mod.Enabled = InStr(1, lvsucursales.SelectedItem.SubItems(1), "No", vbTextCompare) = 0
                Me.del.Enabled = InStr(1, lvsucursales.SelectedItem.SubItems(1), "No", vbTextCompare) = 0
                PopupMenu mnu
            End If
    End Select
End Sub
