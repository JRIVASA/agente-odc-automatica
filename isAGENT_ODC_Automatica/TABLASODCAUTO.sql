USE [vad10]
GO

/****** Object:  Table [dbo].[MA_ODC_PROGRAMADAS]    Script Date: 04/22/2013 15:31:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MA_ODC_PROGRAMADAS](
	[c_sucursal] [nvarchar](15) NOT NULL,
	[c_proveedor] [nvarchar](15) NOT NULL,
	[n_tipo] [int] NOT NULL,
	[n_cada] [int] NOT NULL,
	[n_dia] [int] NOT NULL,
	[n_diassel] [int] NOT NULL,
	[c_usuario] [nvarchar](15) NOT NULL,
	[n_diasreposicion] [int] NOT NULL,
	[n_diasanalisis] [int] NOT NULL,
	[n_tipocosto] [int] NOT NULL,
	[n_diasdespacho] [int] NOT NULL,
	[b_promediar] [bit] NOT NULL,
	[b_descargos] [bit] NOT NULL,
 CONSTRAINT [PK_MA_ODC_PROGRAMADAS] PRIMARY KEY CLUSTERED 
(
	[c_sucursal] ASC,
	[c_proveedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [vad10]
GO

/****** Object:  Table [dbo].[MA_ODC_PROGRAMADAS_DEP]    Script Date: 04/22/2013 15:31:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MA_ODC_PROGRAMADAS_DEP](
	[c_sucursal] [nvarchar](15) NOT NULL,
	[c_proveedor] [nvarchar](15) NOT NULL,
	[c_coddeposito] [nvarchar](15) NOT NULL,
	[b_predeterminado] [bit] NOT NULL,
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO


USE [vad10]
GO

/****** Object:  Table [dbo].[TR_ODC_PROGRAMADAS]    Script Date: 04/22/2013 15:31:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TR_ODC_PROGRAMADAS](
	[c_proveedor] [nvarchar](15) NOT NULL,
	[c_sucursal] [nvarchar](15) NOT NULL,
	[c_documento] [nvarchar](15) NOT NULL,
	[d_fechaexe] [datetime] NOT NULL,
	[b_enviado] [bit] NOT NULL,
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TR_ODC_PROGRAMADAS] ADD  CONSTRAINT [DF_TR_ODC_PROGRAMADAS_d_fechaexe]  DEFAULT (getdate()) FOR [d_fechaexe]
GO


