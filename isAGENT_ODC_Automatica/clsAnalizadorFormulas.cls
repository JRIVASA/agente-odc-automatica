VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAnalizadorFormulas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private Function SignosAgrupamiento(pCadena As String, ByRef pError As Boolean) As String
    
    Dim POS() As Integer
    Dim Valor As String
    Dim MiCadena As String
    Dim Num As Integer
    Dim caracter As String
    Dim Cont As Integer
    Dim CadenaReemplazar As String
    
    Cont = 0
    MiCadena = pCadena
    
Reintentar:
    
    If Not pError Then
        
        On Error GoTo Errores
        
        For Num = 1 To Len(MiCadena)
            
            caracter = Mid(MiCadena, Num, 1)
            
            If caracter = "(" Then
                ReDim Preserve POS(Cont)
                POS(Cont) = Num + 1
                Cont = Cont + 1
            ElseIf caracter = ")" Then
                a = Mid(MiCadena, POS(Cont - 1), Num - (POS(Cont - 1)))
                CadenaReemplazar = Mid(MiCadena, POS(Cont - 1), Num - (POS(Cont - 1)))
                Valor = CStr(CalcularCadena(CadenaReemplazar, pError))
                CadenaReemplazar = Mid(MiCadena, (POS(Cont - 1) - 1), 2 + (Num - (POS(Cont - 1))))
                If Abs(CDbl(Valor)) < 0.9 Then
                    Valor = CDec(Valor)
                End If
                
                MiCadena = Replace(MiCadena, CadenaReemplazar, Valor)
                'MsgBox miCadena
                GoTo Reintentar
            End If
            
        Next Num
        
        SignosAgrupamiento = MiCadena
        
    End If
    
    Exit Function
    
Errores:
    
    MsgBox Err.Description, vbCritical
    pError = True
    
End Function

Private Function CalcularCadena(pCadena As String, ByRef pError As Boolean, Optional MostrarMensaje As Boolean = False) As Double
    
    Dim DatosStr() As String
    Dim POS() As Integer
    Dim Num As Integer
    Dim caracter As String
    Dim Contador As Integer
        
    Valor = 1
    Contador = 0
    
    On Error GoTo Errores
    
    ReDim Preserve DatosStr(0)
    ReDim Preserve POS(0)
    
    For Num = 1 To Len(pCadena)
        
        caracter = Mid(pCadena, Num, 1)
        
        If caracter = "+" Or caracter = "-" Or caracter = "*" Or caracter = "/" Then
            Contador = Contador + 1
            ReDim Preserve DatosStr(Contador)
            ReDim Preserve POS(Contador)
            POS(Contador) = Num
            DatosStr(Contador) = caracter
            Contador = Contador + 1
            ReDim Preserve DatosStr(Contador)
            ReDim Preserve POS(Contador)
        Else
            DatosStr(Contador) = DatosStr(Contador) & caracter
        End If
        
    Next Num
    
    CalcularCadena = CalculoDatos(DatosStr, pError)
    
    Exit Function
    
Errores:
    
    If MostrarMensaje Then
        MsgBox Err.Description, vbCritical
    End If
    
    pError = True
    
End Function

Private Function CalculoDatos(pDatos() As String, ByRef pError As Boolean, Optional MostrarMensaje As Boolean = False) As Double
    
    Dim Cont As Integer
    Dim Num As Integer
    Dim Acum As Double
    Dim Multiplicar As Boolean
    Dim Paso As Boolean
    Dim mFactor As Integer
    Dim PILA() As Double
    
    Num = 0
    Acum = 0
    Paso = False
    Cont = 0
    
    mFactor = 1
    
    On Error GoTo Errores
    
    For Cont = LBound(pDatos) To UBound(pDatos)
        If pDatos(Cont) <> "+" And pDatos(Cont) <> "-" And pDatos(Cont) <> "*" And pDatos(Cont) <> "/" Then
            
            If Paso Then
                Paso = False
                If Multiplicar Then
                    PILA(Num - 1) = (pDatos(Cont) * PILA(Num - 1)) * mFactor
                Else
                    PILA(Num - 1) = (PILA(Num - 1) / pDatos(Cont)) * mFactor
                End If
                
            Else
                ReDim Preserve PILA(Num)
                If IsNumeric(pDatos(Cont)) Then
                    PILA(Num) = CDbl(pDatos(Cont)) * mFactor
                    mFactor = 1
                    Num = Num + 1
                End If
            End If
        Else
             Select Case pDatos(Cont)
                Case "+"
                    mFactor = 1
                Case "-"
                    mFactor = -1 * IIf(IsNumeric(mFactor), mFactor, 1)
                Case "*"
                    Paso = True
                    Multiplicar = True
                    mFactor = 1
                Case "/", "\"
                    Paso = True
                    Multiplicar = False
                    mFactor = 1
            End Select
        End If
    Next Cont
    
    For Cont = LBound(PILA) To UBound(PILA)
        Acum = Acum + PILA(Cont)
    Next Cont
    
    CalculoDatos = Acum
    
    Exit Function
    
Errores:
    
    If MostrarMensaje Then
        MsgBox Err.Description, vbCritical
    End If
    
    pError = True
    
    CalculoDatos = 0
    
End Function

Public Function AnalizadorFormula(pCadena As String) As Double
    
    Dim OcurrioError As Boolean
    Dim CadenaResultante As String
    
    OcurrioError = False
    CadenaResultante = SignosAgrupamiento(pCadena, OcurrioError)
    
    If Not OcurrioError Then
        AnalizadorFormula = CalcularCadena(CadenaResultante, OcurrioError)
    End If
    
End Function
