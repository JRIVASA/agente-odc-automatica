VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsOdcAutoDeposito"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarCodigo                                      As String
Private mvarDescripcion                                 As String
Private mvarSeleccionado                                As Boolean
Private mvarPredeterminado                              As Boolean

Property Get Codigo() As String
    Codigo = mvarCodigo
End Property

Property Get Descripcion() As String
    Descripcion = mvarDescripcion
End Property

Property Get Seleccionado() As Boolean
    Seleccionado = mvarSeleccionado
End Property

Property Let Seleccionado(pValor As Boolean)
    mvarSeleccionado = pValor
End Property

Property Get Predeterminado() As Boolean
    Predeterminado = mvarPredeterminado
End Property

Property Let Predeterminado(pValor As Boolean)
    mvarPredeterminado = pValor
End Property

Public Function AddDeposito(pCodigo As String, pDescri As String, _
pSeleccionado As Boolean, pPredeterminado As Boolean) As clsOdcAutoDeposito
    
    mvarCodigo = pCodigo
    mvarDescripcion = pDescri
    mvarSeleccionado = pSeleccionado
    mvarPredeterminado = pPredeterminado
    
    Set AddDeposito = Me
    
End Function

Public Function AddDepositoRs(pRs As ADODB.Recordset) As clsOdcAutoDeposito
    
    mvarCodigo = pRs!c_coddeposito
    mvarDescripcion = pRs!c_descripcion
    mvarSeleccionado = pRs!Seleccion > 0
    mvarPredeterminado = pRs!Predeterminado = 1
    
    Set AddDepositoRs = Me
    
End Function
