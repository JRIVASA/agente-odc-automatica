Attribute VB_Name = "modAgenteOdc"
Enum eTipoInterfazApp
    etiAppDesconocida
    etiAppAgente
    etiAppDebugOdc
End Enum

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpstring _
As Any, ByVal lpFileName As String) As Long
    
Type AppEstructura
    Conexion                                        As ADODB.Connection
    ArchivoConfig                                   As String
    ArchivoLog                                      As String
    SrvName                                         As String
    SrvUser                                         As String
    SrvPwd                                          As String
    SrvDB                                           As String
    Extensiones                                     As String
    CodigoMail                                      As String
    CarpetaDocumento                                As String
    MailCCO                                         As String
    Mensaje1                                        As String
    Mensaje2                                        As String
    Mensaje3                                        As String
    Firma                                           As String
    RutaImagen                                      As String
    TiempoEspera                                    As Long
    ConnectTimeOut                                  As Long
    CommandTimeOut                                  As Long
    TipoInterfaz                                    As eTipoInterfazApp
End Type

Public AppDatos                                     As AppEstructura
Public Rep                                          As Reporte

Public Moneda_Cod                                   As String
Public Moneda_Fac                                   As Double

Public MonedaFactor                                 As String
Public FactorODC                                    As Double

Public Enum FechaBDPrecision
    FBD_Fecha
    FBD_FechaYMinuto
    FBD_Full
    FBD_HoraYMinuto
    FBD_HoraFull
End Enum

Public Function ObtenerConfiguracion(sIniFile As String, sSection As String, _
sKey As String, sDefault As String) As String
    
    Dim sTemp As String * 10000
    Dim nLength As Integer
    
    sTemp = Space$(10000)
    
    nLength = GetPrivateProfileString(sSection, sKey, sDefault, sTemp, 9999, sIniFile)
    
    ObtenerConfiguracion = Left$(sTemp, nLength)
    
End Function

Public Function ObtenerConfiguracionLong(sIniFile As String, sSection As String, sKey _
As String, sDefault As String) As String
    ObtenerConfiguracionLong = ObtenerConfiguracion(sIniFile, sSection, sKey, sDefault)
End Function

Public Function EscribirConfiguracion(sIniFile As String, sSection As String, sKey _
As String, sValue As String) As String
    
    Dim sTemp As String
    Dim N As Integer
    
    sTemp = sValue
    N = WritePrivateProfileString(sSection, sKey, sValue, sIniFile)
    
End Function

Sub Main()
    
    If App.PrevInstance Then
        Exit Sub
    End If
    
    On Error GoTo Errores
    
    IniciarConfiguracion
    
    If AppDatos.TipoInterfaz <> etiAppDesconocida Then
        
        If CrearConexion(AppDatos.Conexion, AppDatos.CommandTimeOut, AppDatos.ConnectTimeOut, _
        CrearCadenaConexionShape(AppDatos.SrvUser, AppDatos.SrvDB, AppDatos.SrvName, AppDatos.SrvPwd)) Then
            
            If AppDatos.TipoInterfaz = etiAppAgente Then
                IniciarAgente AppDatos.Conexion
            Else
                frmDebug.Show vbModal
            End If
            
        End If
        
    Else
        Err.Raise 10001, , "Tipo de Interfaz no valido"
    End If
    
    End
    
    Exit Sub
    
Errores:
    
    EscribirError Err
    
End Sub

Private Sub IniciarConfiguracion()
    
    Dim mAux As Long
    
    With AppDatos
        
        .ArchivoConfig = App.path & "\Config.ini"
        .ArchivoLog = App.path & "\AgenteOdcLog.log"
        
        mAux = Val(ObtenerConfiguracion(.ArchivoConfig, "Config", "TipoInterfaz", ""))
        
        If mAux > 0 And mAux <= 2 Then
           .TipoInterfaz = mAux
        Else
           .TipoInterfaz = etiAppDesconocida
        End If
        
        .TiempoEspera = Val(ObtenerConfiguracion(.ArchivoConfig, "Config", "TiempoEsperaRpt", 2))
        
        If .TiempoEspera > 100 Then
            .TiempoEspera = 2
        End If
        
        .Extensiones = ObtenerConfiguracion(.ArchivoConfig, "Config", "ExtensionesValidas", "COM")
        
        .SrvDB = ObtenerConfiguracion(.ArchivoConfig, "Server", "DBName", "VAD10")
        .SrvName = ObtenerConfiguracion(.ArchivoConfig, "Server", "Server", "")
        .SrvPwd = ObtenerConfiguracion(.ArchivoConfig, "Server", "Pwd", Empty)
        .SrvUser = ObtenerConfiguracion(.ArchivoConfig, "Server", "User", "SA")
        .CommandTimeOut = Val(ObtenerConfiguracion(.ArchivoConfig, "Server", "CommandTimeOut", ""))
        .ConnectTimeOut = Val(ObtenerConfiguracion(.ArchivoConfig, "Server", "ConnectionTimeOut", ""))
        
        .CodigoMail = ObtenerConfiguracion(.ArchivoConfig, "Config", "CodigoMail", "")
        
        .CarpetaDocumento = ObtenerConfiguracion(.ArchivoConfig, "Config", _
        "CarpetaArchivo", App.path & "\ODC\")
        
        .MailCCO = ObtenerConfiguracion(.ArchivoConfig, "Mail", "CopiaOculta", Empty)
        
        .Mensaje1 = ObtenerConfiguracionLong(.ArchivoConfig, "Mail", "Mensaje1", Empty)
        .Mensaje2 = ObtenerConfiguracionLong(.ArchivoConfig, "Mail", "Mensaje2", Empty)
        .Mensaje3 = ObtenerConfiguracionLong(.ArchivoConfig, "Mail", "Mensaje3", Empty)
        
        .Firma = ObtenerConfiguracionLong(.ArchivoConfig, "Mail", "Firma", Empty)
        .RutaImagen = ObtenerConfiguracion(.ArchivoConfig, "Mail", "RutaImagen", Empty)
        
    End With
    
End Sub

Private Function CrearCadenaConexion(pUser As String, pBaseDatos As String, _
pServidor As String, pUserPwd As String) As String
    
    Dim MiCadena As String
    
    MiCadena = "Provider=SQLOLEDB.1;Persist Security Info=False; " _
    & "User ID=" & pUser & ";Password=" & pUserPwd & ";Initial Catalog=" & pBaseDatos _
    & ";Data Source=" & pServidor
    
    ' Debug.Print miCadena
    
    CrearCadenaConexion = MiCadena
    
End Function

Public Function CrearCadenaConexionShape(pUser As String, pBaseDatos As String, pServidor As String, pUserPwd As String) As String
    CrearCadenaConexionShape = "Provider=MSDataShape.1;Persist Security Info=false;" _
    & "Connect Timeout=5;Data Source=" & pServidor & ";User ID=" & pUser & ";Password=" & pUserPwd & ";" _
    & ";Initial Catalog=" & pBaseDatos & ";Data Provider=SQLOLEDB.1"
End Function

Private Function CrearConexion(ByRef pCn, pCmdTimeOut As Long, pCnTimeOut As Long, pCadena) As Boolean
    
    On Error GoTo Err_Manager
    
    Set pCn = Nothing
    Set pCn = New ADODB.Connection
    pCn.CommandTimeOut = pCmdTimeOut
    pCn.ConnectionTimeout = pCnTimeOut
    pCn.CursorLocation = adUseServer
    pCn.Open pCadena
    CrearConexion = True
    Exit Function
    
Err_Manager:
   
    CrearConexion = False
    EscribirError Err, "Creando Conexiones"
    
End Function

Public Sub EscribirError(pError As ErrObject, Optional pDetalles As String = "")
    
    Dim mCanal As Integer
    
    'MsgBox Err.Description
    mCanal = FreeFile()
    Open AppDatos.ArchivoLog For Append Access Write As #mCanal
    Print #mCanal, Now & ",Error (" & pError.Number & ") " & pError.Description & IIf(Trim(pDetalles) <> "", ",", "") & pDetalles
    Close #mCanal
    
End Sub

Private Sub IniciarAgente(pCn As ADODB.Connection)
    
    Dim mCls As clsOdcAuto
    
    Set mCls = New clsOdcAuto
    
    mCls.IniciarAgenteOdc pCn
    
    Set mCls = Nothing
    
End Sub

Public Function BuscarReglaNegocioStr(pCn As ADODB.Connection, pCampo, _
Optional pDefault As String = "") As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
    
    On Error GoTo Errores
    
    mSQL = "Select * from ma_reglasdenegocio where campo = '" & pCampo & "' "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarReglaNegocioStr = CStr(mRs!Valor)
    Else
        BuscarReglaNegocioStr = pDefault
    End If
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Public Function BuscarMonedapredeterminada(pCn As ADODB.Connection, Optional ByRef pFactor As Double = -1) As String
    
    Dim mRs As New ADODB.Recordset
    
    'Cambio para calculo de factorizacion dependiendo de la moneda que venga del proveedor
    
    'mRs.Open "Select * MA_MONEDAS inner join MA_PROVEEDORES on MA_PROVEEDORES.c_Cod_Moneda_ODC_Auto = MA_MONEDAS.c_codmonedas where MA_PROVEEDORES.c_codproveed = '" & CodProveedor & "' ", _
    'pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    mRs.Open "Select * from MA_MONEDAS where b_preferencia=1", _
    pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        If pFactor <> -1 Then pFactor = mRs!n_Factor
        BuscarMonedapredeterminada = mRs!c_CodMoneda
    End If
    
End Function

Public Function BuscarCorrelativoStellar(pCn As ADODB.Connection, pCampo As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    
    Set mRs = New ADODB.Recordset
    
    mSQL = "Select * from ma_correlativos where cu_campo = '" & pCampo & "' "
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockOptimistic, adCmdText
    
    If Not mRs.EOF Then
        mRs!nu_valor = Val(mRs!nu_valor) + 1
        BuscarCorrelativoStellar = Format(mRs!nu_valor, "00000000")
        mRs.Update
    End If
    
    Set mRs = Nothing
    
End Function

Public Function ManejaSucursales(pCn As ADODB.Connection) As Boolean
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    
    Set mRs = New ADODB.Recordset
    
    mSQL = "SELECT  count(*) as nSucursales FROM  MA_SUCURSALES WHERE  (c_estado = 1)"
    
    mRs.CursorLocation = adUseClient
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly
    
    ManejaSucursales = mRs!nSucursales > 1
    
    Set mRs = Nothing
    
End Function

Public Sub Apertura_RecordsetC(ByRef pRs As ADODB.Recordset)
    Set pRs = New ADODB.Recordset
    pRs.CursorLocation = adUseClient
End Sub

Public Sub Cerrar_Recordset(ByRef pRs As ADODB.Recordset)
    Set pRs = Nothing
End Sub

Public Function FormatearCadena(CadenaText As String, CadenaFormato As String) As String
    
    ' Limpia la CadenaText de que no tenga salto de carros
    
    CadenaText = Replace(CadenaText, Chr(13), " ")
    CadenaText = Replace(CadenaText, Chr(10), " ")
    CadenaText = Trim(CadenaText)
       
    If Mid(CadenaFormato, 2, 1) = " " And Mid(CadenaFormato, Len(CadenaFormato) - 1, 1) = " " Then
        
        'A = Mid(CadenaFormato, Len(CadenaFormato) - 1, 1)
        Long_Formato = Len(CadenaFormato)
        Long_Texto = Len(CadenaText)
        
        If CDbl(Long_Formato) > CDbl(Long_Texto) Then
             Spacios = CInt((CDbl(Long_Formato) - CDbl(Long_Texto)))
             'FormatearCadena = Space(Len(CadenaFormato) - Len(Trim(CadenaText))) & CadenaText
             FormatearCadena = Space(Spacios) & CadenaText
             a = Len(FormatearCadena)
        Else
            FormatearCadena = CadenaText
        End If
        
        Exit Function
        
    End If
  
    If Mid(CadenaFormato, 2, 1) = " " Then ' se alinea a la derecha
        If Len(Trim(CadenaText)) <= Len(CadenaFormato) Then
            FormatearCadena = Space(Len(CadenaFormato) - Len(Trim(CadenaText))) & CadenaText
        Else
            FormatearCadena = CadenaText
        End If
    Else 'de otra forma a la izquierda
        If Len(Trim(CadenaText)) >= Len(CadenaFormato) Then
           FormatearCadena = Mid(Trim(CadenaText), 1, Len(CadenaFormato))
        Else
           FormatearCadena = CadenaText & Space(Len(CadenaFormato) - Len(Trim(CadenaText)))
          
        End If
        c = Len(FormatearCadena)
        d = Len(CadenaFormato)
    End If
  
End Function

Function NumPaginas(LinRepeRepor As Integer, LinRepeCons As Integer) As Integer
    
    Dim var1 As Integer
    Dim var2 As Integer
    
    var1 = LinRepeCons Mod LinRepeRepor
    
    If var1 > 0 Then
        NumPaginas = Int(LinRepeCons / LinRepeRepor) + 1
    Else
        NumPaginas = LinRepeCons / LinRepeRepor
    End If

End Function

Public Sub mensaje(pMostrar As Boolean, pCadena As String)
    
    Dim mCanal As Integer
    
    On Error GoTo Errores
    mCanal = FreeFile()
    Open App.path & "\AgentLog.log" For Append Access Write As #mCanal
    Print #mCanal, Now & ", " & pCadena
    Close #mCanal
    Exit Sub
    
Errores:
    
    Err.Clear
    Reset
    
End Sub

Public Sub REPO_CABE(ByRef Reporte, Titulo, Optional pMayuscula As Boolean = True)
    On Error Resume Next
    Dim rs As New ADODB.Recordset, RsSucursal As New ADODB.Recordset
    Call Apertura_RecordsetC(rs)
    rs.Open "select * from vad10.dbo.ESTRUC_SIS", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    Call Apertura_RecordsetC(RsSucursal)
    RsSucursal.Open "SELECT * FROM MA_SUCURSALES WHERE C_CODIGO = '" & Sucursal & "'", Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
'    Far.strEmpresa = rs!nom_org
    Reporte.Caption = Titulo
    If Not rs.EOF Then
        Reporte.Sections("enc_pag").Controls("lbl_empresa").Caption = rs!Nom_Org
        Reporte.Sections("enc_pag").Controls("lbl_tit_rep").Caption = IIf(pMayuscula, UCase(Titulo), Titulo)
        Reporte.Sections("enc_pag").Controls("paginas").Caption = "Pg %p de %P"
    End If
    If Not RsSucursal.EOF Then
        Reporte.Sections("enc_pag").Controls("lbl_localidad").Caption = "Localidad:" & RsSucursal!c_Descripcion
    End If
    Call Cerrar_Recordset(rs)
    Call Cerrar_Recordset(RsSucursal)
    Err.Clear
End Sub

Public Function buscar_usuario(pCn As ADODB.Connection, pCodigo) As String
    
    Dim mRs As New ADODB.Recordset
    Dim mSQL As String
   
    mSQL = "Select descripcion from ma_usuarios where codusuario='" & pCodigo & "'"
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        buscar_usuario = mRs!Descripcion
    End If

End Function

Public Function ExisteCampoTabla(pCampo As String, Optional pRs, Optional pSql As String = "", Optional pCn As ADODB.Connection) As Boolean
    Dim mRs As ADODB.Recordset
    
    On Error GoTo Errores
    If Not IsMissing(pRs) Then
        mAux = pRs.Fields(pCampo).value
        ExisteCampoTabla = True
    Else
       Set mRs = New ADODB.Recordset
       mRs.Open pSql, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
       ExisteCampoTabla = True
       mRs.Close
    End If
    Exit Function
    
Errores:
   
    Err.Clear
    ExisteCampoTabla = False
End Function

Public Function BuscarNombreDeposito(pCn As ADODB.Connection, pCodigo As String) As String
    
    Dim mRs As ADODB.Recordset
    
    Apertura_RecordsetC mRs
    mRs.Open _
    "select * from MA_DEPOSITO " & _
    "where c_coddeposito = '" & pCodigo & "' ", _
    pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarNombreDeposito = mRs!c_Descripcion
    End If
    
    Cerrar_Recordset mRs
    
End Function

Public Function BuscarCodigoEdiProducto(pCn As ADODB.Connection, pCodNasa As String) As String
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    
    Apertura_RecordsetC mRs
    
    mSQL = "Select * from ma_codigos " & _
    "where c_codnasa = '" & pCodNasa & "' " & _
    "and nu_intercambio = 1 "
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarCodigoEdiProducto = mRs!c_Codigo
    Else
        BuscarCodigoEdiProducto = pCodNasa
    End If
    
    Cerrar_Recordset mRs
    
End Function

Public Function BuscarExistenciaProducto(pCn As ADODB.Connection, pCodigo) As Double
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    
    Apertura_RecordsetC mRs
    
    mSQL = "Select sum(n_cantidad) as Existencia " & _
    "from MA_DEPOPROD " & _
    "where c_codarticulo = '" & pCodigo & "' "
    
    mRs.Open mSQL, pCn, adOpenDynamic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarExistenciaProducto = IIf(IsNull(mRs!Existencia), 0, mRs!Existencia)
    End If
    
    Cerrar_Recordset mRs
    
End Function

Public Function ExisteTabla(pCn As ADODB.Connection, pTabla As String, Optional pBd As String = "")
    
    Dim mRs As ADODB.Recordset
    Dim mSQL As String
    Dim mBd As String
    
    If pBd <> "" Then
        mBd = pCn.DefaultDatabase
        pCn.DefaultDatabase = pBd
    End If
    
    Set mRs = New ADODB.Recordset
    
    mSQL = "Select * from sysobjects " & _
    "where name = '" & pTabla & "' " & _
    "and xtype='U' "
    
    mRs.Open mSQL, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    ExisteTabla = Not mRs.EOF
    
    If pBd <> "" Then
        pCn.DefaultDatabase = mBd
    End If
    
    Set mRs = Nothing
    
End Function

Public Sub EnviarDocumento(pCn As ADODB.Connection, pRsCorreo As ADODB.Recordset, _
pEmpresa As String, pNumeroOdc As String, _
Optional pProveedorDes As String)
    
    Dim RsMail As ADODB.Recordset
    Dim Obj_Email As CDO.Message
    Dim FSO As Object
    
    Set RsMail = pCn.Execute( _
    "SELECT * FROM MA_CONFIGURACION_MAIL " & _
    "WHERE cu_codigo = '" & AppDatos.CodigoMail & "' ")
    
    Set FSO = CreateObject("Scripting.FileSystemObject")
    
    If RsMail.EOF Then
        
        Call mensaje(True, "No se puede realizar el env�o. " & _
        "No se encontr� ninguna configuraci�n de correo electr�nico.")
        
        Exit Sub
        
    Else
        
        On Error GoTo Errores
        
        Set poSendMail = New vbSendMail.clsSendMail
        
        With poSendMail
            
            Screen.MousePointer = vbHourglass
            mFecha = Now
            
            Do While DateDiff("s", mFecha, Now) < AppDatos.TiempoEspera
            Loop
            
            Set Obj_Email = New CDO.Message
            
            If .IsValidEmailAddress(pRsCorreo!MailAdministrativo) Then
                
                DESTINATARIO = Trim(pRsCorreo!nombreAdministrativo) & _
                " <" & Trim(pRsCorreo!MailAdministrativo) & ">"
                
            Else
                
                mensaje True, "Correo Invalido (" & pRsCorreo!MailAdministrativo & ") " & _
                "proveedor " & pRsCorreo!proveedor
                
                Exit Sub
                
            End If
            
            Obj_Email.Configuration.Fields(cdoSMTPServer) = RsMail!cu_servidor
            Obj_Email.Configuration.Fields(cdoSendUsingMethod) = 2

            Obj_Email.Configuration.Fields.Item( _
            "http://schemas.microsoft.com/cdo/configuration/smtpserverport") = CInt(RsMail!cu_puerto)
            Obj_Email.Configuration.Fields.Item( _
            "http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = RsMail!bu_autenticacion
            Obj_Email.Configuration.Fields.Item( _
            "http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 30
            Obj_Email.Configuration.Fields.Item( _
            "http://schemas.microsoft.com/cdo/configuration/sendusername") = RsMail!cu_login
            Obj_Email.Configuration.Fields.Item( _
            "http://schemas.microsoft.com/cdo/configuration/sendpassword") = RsMail!cu_password
            Obj_Email.Configuration.Fields.Item( _
            "http://schemas.microsoft.com/cdo/configuration/smtpusessl") = True
            
            Obj_Email.From = Trim(RsMail!cu_nombreEmisor) & " <" & Trim(RsMail!cu_correoEmisor) & ">"
            
            If .IsValidEmailAddress(pRsCorreo!mailRepresentante) Then
                msgCC = msgCC & Trim(pRsCorreo!nombreRepresentante) & " <" & Trim(pRsCorreo!mailRepresentante) & ">;"
            End If

            If .IsValidEmailAddress(pRsCorreo!mailregional) Then
                msgCC = msgCC & Trim(pRsCorreo!nombreRegional) & " <" & Trim(pRsCorreo!mailregional) & ">;"
            End If

            If .IsValidEmailAddress(pRsCorreo!mailventas) Then
                msgCC = msgCC & Trim(pRsCorreo!nombreVentas) & " <" & Trim(pRsCorreo!mailventas) & ">;"
            End If
            
            If .IsValidEmailAddress(pRsCorreo!mailVendedor) Then
                msgCC = msgCC & Trim(pRsCorreo!nombreVendedor) & " <" & Trim(pRsCorreo!mailVendedor) & ">;"
            End If
            
            Obj_Email.To = DESTINATARIO
            
            If msgCC <> vbNullString Then
                msgCC = Mid(msgCC, 1, CInt(Len(msgCC)) - 1)
                Obj_Email.CC = msgCC
            End If
            
            If AppDatos.MailCCO <> vbNullString Then
                Obj_Email.BCC = AppDatos.MailCCO
            End If
            
            Obj_Email.Subject = "Orden de Compra Stellar N� " & pNumeroOdc & " - " & pEmpresa
            
            Dim ProvDesc As String
            
            pProveedorDes = isDBNull(pProveedorDes, vbNullString)
            
            ProvDesc = IIf(pProveedorDes <> vbNullString, "Sres. " & pProveedorDes & "<br><br>", "")
            
            If AppDatos.Mensaje1 = vbNullString _
            Or AppDatos.Mensaje2 = vbNullString _
            Or AppDatos.Mensaje3 = vbNullString Then
                Obj_Email.HTMLBody = ProvDesc & "Adjunto Orden de Compra autom�tica No " & pNumeroOdc & vbNewLine & vbNewLine & "<html><br><br><br><br><br><font style='color:#7F7F7F'>Este correo es generado de manera autom�tica por la plataforma de informaci�n Stellar isBUSINESS (www.mistellar.com)" _
                & " Utilizando una cuenta de correo no monitoreada. Por favor, no responda o reenv�e mensajes a esta direcci�n. Si presenta alguna duda, " _
                & "por favor comun�quese a las oficinas de " & pEmpresa & "</font></html>"
            Else
                'Debug.Print appDatos.Mensaje1
                'Open "C:\ConsultaNicolinica.TXT" For Output As #1
                'Print #1, "<html>" & appDatos.Mensaje1 & "<br><br>" & appDatos.Mensaje2 & "<br><br>" & appDatos.Mensaje3 & "<br><br><br><br><br>" & appDatos.Firma & "</html>"
                Obj_Email.HTMLBody = "<html>" & ProvDesc & AppDatos.Mensaje1 & "<br><br>" & AppDatos.Mensaje2 & "<br><br>" & AppDatos.Mensaje3 & "<br><br><br><br><br>" & AppDatos.Firma & "</html>"
            End If
            
'            Obj_Email.TextBody = "Adjunto Orden de Compra autom�tica No " & pNumeroOdc & vbCrLf & "Este correo es generado de manera autom�tica por la plataforma Stellar isBUSINESS." _
'                                & " Utilizando una cuenta de correo no monitoreada. Por favor, no responda o reenv�e mensajes a esta direcci�n. Si presenta alguna duda o desea realizar " _
'                                & "una sugerencia, por favor comun�quese a nuestras oficinas."
            'Debug.Print App.path & "\" & appDatos.rutaImagen
            
            For i = 1 To 20
                
                If Not Dir(AppDatos.CarpetaDocumento & "documento-" & "P" & i & ".pdf") = "" Then
                    Obj_Email.AddAttachment (AppDatos.CarpetaDocumento & "documento-" & "P" & i & ".pdf")
                Else
                    Exit For
                End If
                
            Next i
            
            'Obj_Email.AddAttachment (appDatos.CarpetaDocumento & "documento.pdf")
            
            If FSO.fileexists(App.path & "\" & AppDatos.RutaImagen) Then
                Obj_Email.AddAttachment (App.path & "\" & AppDatos.RutaImagen)
            Else
                If AppDatos.RutaImagen <> "" Then
                    EscribirError Err, "No se encontr� la imagen configurada en el archivo config.ini. "
                End If
            End If
            
            Obj_Email.Configuration.Fields.Update
            Obj_Email.Send
            
            MarcarEnviada pCn, pRsCorreo
            
            EliminarDocumento
            
        End With
        
    End If
    
    Exit Sub
    
Errores:
    
    'MsgBox Err.Description
    Debug.Print Err.Description
    Err.Clear
    EliminarDocumento
    
End Sub

Private Sub MarcarEnviada(pCn As ADODB.Connection, pRsCorreo As ADODB.Recordset)
    
    Dim mSQL As String
    
    mSQL = "update TR_ODC_PROGRAMADAS set " & _
    "b_enviado = 1 " & _
    "where id = " & pRsCorreo!id & " "
    
    pCn.Execute mSQL
    
End Sub

Private Sub EliminarDocumento()
    
    On Error Resume Next
    
    'Kill (appDatos.CarpetaDocumento & "documento.pdf")
    Kill (AppDatos.CarpetaDocumento & "*.pdf")
    
    Debug.Print Err.Description
    
    Err.Clear
    
End Sub

' Asigna el valor de una variable sin importar si es objeto o no
' ignorando la palabra SET

Public Sub SafeVarAssign(ByRef pVar, ByVal pValue)
    On Error Resume Next
    Set pVar = pValue
    If Err.Number = 0 Then Exit Sub
    pVar = pValue
End Sub

' Sintaxis para obtener un elemento �ndice de un objeto de manera segura,
' Retornando un Valor por Defecto en caso de que el elemento no exista,
' Para evitar errores en tiempo de ejecuci�n y no tener que separar las instrucciones en IFs.

' Ejemplo, Caso Recordset

' Cambiaria:

'If ExisteCampoTabla(Campo, Recordset) Then
'   Variable = Recordset!Campo
'Else
'   Variable = 0
'End If

' Por:

' Variable = sProp(Recordset, Campo, 0)
' pObj(pItem) ser�a equivalente a regresar : pObj.Fields(pItem).Value)

' Todos los par�metros tanto de Entrada/Salida son Variant (Acepta Cualquier Cosa)

Public Function SafeItem(pObj, pItem, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeItem, pObj(pItem)
    Exit Function
Default:
    SafeVarAssign SafeItem, pDefaultValue
End Function

' Mismo prop�sito que la anterior, SafeItem()
' Solo que con la sintaxis para acceder a la propiedad de un objeto
' en vez de a un elemento de una colecci�n.

' Quizas para uso futuro.

Public Function SafeProp(pObj, pProperty, pDefaultValue)
    On Error GoTo Default
    SafeVarAssign SafeProp, CallByName(pObj, pProperty, VbGet)
    Exit Function
Default:
    SafeVarAssign SafeProp, pDefaultValue
End Function

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a un elemento.

Public Sub SafeItemAssign(pObj, pItem, pValue)
    On Error Resume Next
    Set pObj(pItem) = pValue
    If Err.Number = 0 Then Exit Sub
    pObj(pItem) = pValue
End Sub

' Misma filosof�a de SafeItem(), pero para asignar en vez de devolver, asignar valor a una propiedad.

Public Sub SafePropAssign(pObj, pProperty, pValue)
    On Error Resume Next
    CallByName pObj, pProperty, VbLet, pValue
End Sub

Public Function isDBNull(ByVal pValue, Optional pDefaultValueReturned) As Variant

    If Not IsMissing(pDefaultValueReturned) Then
        ' Return Value
        If IsNull(pValue) Then
            isDBNull = pDefaultValueReturned
        Else
            SafeVarAssign isDBNull, pValue
        End If
    Else
        isDBNull = IsNull(pValue) 'Return Check
    End If

End Function

Public Function BuscarValorBD(ByVal Campo As String, ByVal SQL As String, _
Optional ByVal pDefault = "", Optional pCn As ADODB.Connection) As Variant
    
    On Error GoTo Err_Campo
    
    Dim mRs As ADODB.Recordset: Set mRs = New ADODB.Recordset
    
    mRs.Open SQL, pCn, adOpenStatic, adLockReadOnly, adCmdText
    
    If Not mRs.EOF Then
        BuscarValorBD = mRs.Fields(Campo).value
    Else
        BuscarValorBD = pDefault
    End If
    
    mRs.Close
    
    Exit Function
    
Err_Campo:
    
    'Debug.Print Err.Description
    
    Err.Clear
    BuscarValorBD = pDefault
    
End Function

Public Function FactorizarPrecio(ByVal pCn, _
ByVal pValor As Double, _
Optional ByVal pFactor As Double = -1, _
Optional ByRef pCodMonedaOrigen As String, _
Optional ByVal pFactorizarMoneda As String = "")
    
    If pFactor > 0 And pFactorizarMoneda = Empty Then
        
        FactorizarPrecio = Round(pValor * pFactor, 8)
        
    ElseIf Trim(pCodMonedaOrigen) <> Empty Then
        
        Dim mCn As ADODB.Connection
        Set mCn = pCn
        
        If pFactorizarMoneda <> Empty Then
            
            TmpMoneda_Sim = BuscarValorBD("c_Simbolo", "SELECT c_Simbolo FROM MA_MONEDAS " & _
            "WHERE c_CodMoneda = '" & pCodMonedaOrigen & "'", , mCn)
            mFactorOrigen = BuscarValorBD("n_Factor", "SELECT n_Factor FROM MA_MONEDAS " & _
            "WHERE c_CodMoneda = '" & pCodMonedaOrigen & "'", 1, mCn)
            
            If pFactor > 0 Then
                mFactorDestino = pFactor
            Else
                TmpMoneda_Dec = BuscarValorBD("n_Decimales", "SELECT n_Decimales FROM MA_MONEDAS " & _
                "WHERE c_CodMoneda = '" & pCodMonedaOrigen & "'", , mCn)
                mFactorDestino = BuscarValorBD("n_Factor", "SELECT n_Factor FROM MA_MONEDAS " & _
                "WHERE c_CodMoneda = '" & pCodMonedaOrigen & "'", 1, mCn)
            End If
            
            If mFactorOrigen > 0 And mFactorDestino > 0 _
            And pCodMonedaOrigen <> pFactorizarMoneda Then
                FactorizarPrecio = CDec(pValor * (CDec(mFactorOrigen) / CDec(mFactorDestino)))
            Else
                FactorizarPrecio = pValor
            End If
            
        ElseIf pCodMonedaOrigen <> Moneda_Cod _
        And pCodMonedaOrigen <> Empty Then
            
            TmpMoneda_Sim = BuscarValorBD("c_Simbolo", "SELECT c_Simbolo FROM MA_MONEDAS " & _
            "WHERE c_CodMoneda = '" & pCodMonedaOrigen & "'", , mCn)
            mFactorTmp = BuscarValorBD("n_Factor", "SELECT n_Factor FROM MA_MONEDAS " & _
            "WHERE c_CodMoneda = '" & pCodMonedaOrigen & "'", 1, mCn)
            
            If mFactorTmp > 0 Then
                'SE divide para pasar a bolivares
                FactorizarPrecio = Round(pValor / CDbl(mFactorTmp), 8)
            Else
                FactorizarPrecio = pValor
            End If
            
        Else
            FactorizarPrecio = pValor
            TmpMoneda_Sim = Moneda_Simbolo
        End If
        
    End If
    
End Function

Public Function FechaBD(ByVal Expression, _
Optional pTipo As FechaBDPrecision = FBD_Fecha, _
Optional pGrabarRecordset As Boolean = False) As String
    
    If Not pGrabarRecordset Then
    
        Select Case pTipo
        
            Case FBD_Fecha
            
                FechaBD = Format(Expression, "YYYYMMDD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
            
                FechaBD = Format(Expression, "YYYYMMDD HH:mm")
                
            Case FBD_Full
                
                Expression = CDate(Expression)
            
                Dim dDate As Date
                Dim dMilli As Double
                Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYYMMDD HH:mm:ss") & Format(lMilli / 1000, ".000")
            
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
            
                FechaBD = Format(Expression, "HH:mm")
            
            Case FBD_HoraFull
                
                FechaBD = Right(FechaBD(Expression, FBD_Full), 12)
                
        End Select
    
    Else
        
        Select Case pTipo
            
            Case FBD_Fecha
                
                FechaBD = Format(Expression, "YYYY-MM-DD")
                
            Case FBD_FechaYMinuto ' Si es para filtrar, requiere comparar contra CAST(Campo AS SmallDateTime)
                
                FechaBD = Format(Expression, "YYYY-MM-DD HH:mm")
                
            Case FBD_Full
                
                Expression = CDate(Expression)
            
                'Dim dDate As Date
                'Dim dMilli As Double
                'Dim lMilli As Long
                
                dDate = DateSerial(Year(Expression), Month(Expression), Day(Expression)) + TimeSerial(Hour(Expression), Minute(Expression), Second(Expression))
                dMilli = Expression - dDate
                lMilli = dMilli * 86400000
                
                If lMilli < 0 Then
                   lMilli = lMilli + 1000 ' Was rounded so add 1 second
                   dDate = DateAdd("s", -1, dDate) ' Was rounded so subtract 1 second
                End If
                
                FechaBD = Format(dDate, "YYYY-MM-DD HH:mm:ss") '& Format(lMilli / 1000, ".000")
                
            Case FBD_HoraYMinuto ' Si es para filtrar, es ideal comparar contra CAST(Campo AS TIME)
                
                FechaBD = Format(Expression, "HH:mm")
                
            Case FBD_HoraFull
                
                FechaBD = Format(Expression, "HH:mm:ss") 'Right(FechaBD(Expression, FBD_Full), 12)
                ' El Formato de Hora Full (Con MS) No funciona de manera confiable en recordset.
                ' Si a�n as� se desea utilizar este formato, volver a llamar a esta funci�n sin pGrabarRecordset
                
        End Select
        
    End If
    
End Function
