VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frmConfig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Configuracion"
   ClientHeight    =   7560
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5430
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   5430
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Caption         =   "Depositos"
      Height          =   2295
      Left            =   120
      TabIndex        =   29
      Top             =   4680
      Width           =   5175
      Begin MSComctlLib.ListView lvDeposito 
         Height          =   1815
         Left            =   120
         TabIndex        =   30
         Top             =   360
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   3201
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         OLEDropMode     =   1
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         OLEDropMode     =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Deposito"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   2
            SubItemIndex    =   1
            Text            =   "Pred"
            Object.Width           =   1764
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Reposicion"
      Height          =   1695
      Left            =   120
      TabIndex        =   20
      Top             =   2880
      Width           =   5175
      Begin VB.CheckBox chkdescargos 
         Alignment       =   1  'Right Justify
         Caption         =   "Considerar Descargos"
         Height          =   255
         Left            =   2880
         TabIndex        =   32
         Top             =   1320
         Width           =   2055
      End
      Begin VB.CheckBox chkPromediar 
         Alignment       =   1  'Right Justify
         Caption         =   "Promediar Ventas"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   1320
         Width           =   1695
      End
      Begin VB.TextBox txtDiasA 
         Height          =   285
         Left            =   1200
         MaxLength       =   4
         TabIndex        =   28
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox txtDiasR 
         Height          =   285
         Left            =   4320
         MaxLength       =   4
         TabIndex        =   27
         Top             =   960
         Width           =   615
      End
      Begin VB.TextBox txtDiasD 
         Height          =   285
         Left            =   4320
         MaxLength       =   4
         TabIndex        =   26
         Top             =   480
         Width           =   615
      End
      Begin VB.ComboBox cboCosto 
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label lblConfig 
         Caption         =   "Tipo de Costo"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   24
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblConfig 
         Caption         =   "Dias Despachar"
         Height          =   255
         Index           =   6
         Left            =   3000
         TabIndex        =   23
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label lblConfig 
         Caption         =   "Dias Reposicion"
         Height          =   255
         Index           =   4
         Left            =   3000
         TabIndex        =   22
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label lblConfig 
         Caption         =   "Dias Analisis"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   21
         Top             =   960
         Width           =   1095
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancelar"
      Height          =   375
      Left            =   4440
      TabIndex        =   19
      Top             =   7080
      Width           =   855
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   3480
      TabIndex        =   18
      Top             =   7080
      Width           =   855
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ejecucion"
      Height          =   2655
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5175
      Begin VB.Frame Frame2 
         Height          =   1575
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   4935
         Begin VB.TextBox txteldia 
            Height          =   285
            Left            =   720
            MaxLength       =   2
            TabIndex        =   16
            Top             =   240
            Width           =   390
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Domingo"
            Height          =   375
            Index           =   0
            Left            =   120
            TabIndex        =   14
            Top             =   600
            Width           =   1095
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Lunes"
            Height          =   375
            Index           =   1
            Left            =   1440
            TabIndex        =   13
            Top             =   600
            Width           =   735
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Martes"
            Height          =   375
            Index           =   2
            Left            =   2640
            TabIndex        =   12
            Top             =   600
            Width           =   855
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Miercoles"
            Height          =   375
            Index           =   3
            Left            =   3720
            TabIndex        =   11
            Top             =   600
            Width           =   975
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Jueves"
            Height          =   375
            Index           =   4
            Left            =   120
            TabIndex        =   10
            Top             =   1080
            Width           =   855
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Viernes"
            Height          =   375
            Index           =   5
            Left            =   1440
            TabIndex        =   9
            Top             =   1080
            Width           =   855
         End
         Begin VB.CheckBox Check1 
            Caption         =   "Sabado"
            Height          =   375
            Index           =   6
            Left            =   2640
            TabIndex        =   8
            Top             =   1080
            Width           =   975
         End
         Begin MSComCtl2.UpDown UpDown2 
            Height          =   285
            Left            =   1110
            TabIndex        =   17
            Top             =   240
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   503
            _Version        =   393216
            Value           =   1
            BuddyControl    =   "txteldia"
            BuddyDispid     =   196622
            OrigLeft        =   1080
            OrigTop         =   240
            OrigRight       =   1335
            OrigBottom      =   495
            Max             =   30
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.Label lblConfig 
            Caption         =   "El Dia"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Width           =   495
         End
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   285
         Left            =   3750
         TabIndex        =   5
         Top             =   480
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   503
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtDias"
         BuddyDispid     =   196624
         OrigLeft        =   3720
         OrigTop         =   480
         OrigRight       =   3975
         OrigBottom      =   735
         Max             =   360
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
      Begin VB.TextBox txtDias 
         Height          =   285
         Left            =   3360
         MaxLength       =   3
         TabIndex        =   4
         Top             =   480
         Width           =   390
      End
      Begin VB.ComboBox cboFrecuencia 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   480
         Width           =   1455
      End
      Begin VB.Label lblConfig 
         Caption         =   "Dias"
         Height          =   255
         Index           =   2
         Left            =   4080
         TabIndex        =   7
         Top             =   480
         Width           =   855
      End
      Begin VB.Label lblConfig 
         Caption         =   "Cada"
         Height          =   255
         Index           =   1
         Left            =   2880
         TabIndex        =   3
         Top             =   480
         Width           =   495
      End
      Begin VB.Label lblConfig 
         Caption         =   "Frecuencia"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fCls As clsOdcAutoConfig

Private Sub cboFrecuencia_Click()
    
    Me.txteldia.Visible = cboFrecuencia.ListIndex <> eOdcAutoSemanal
    Me.lblConfig(3).Visible = cboFrecuencia.ListIndex <> eOdcAutoSemanal
    Me.Frame2.Visible = cboFrecuencia.ListIndex <> eOdcAutoDiaria
    Me.UpDown2.Visible = cboFrecuencia.ListIndex <> eOdcAutoSemanal
    Me.lblConfig(2).Caption = BuscarDescriLabel(cboFrecuencia.ListIndex)
    
    For i = 0 To Check1.Count - 1
        Check1(i).Visible = cboFrecuencia.ListIndex = eOdcAutoSemanal
        Check1(i).value = ValidarDiaSemana(CInt(i)) And cboFrecuencia.ListIndex = eOdcAutoSemanal
    Next
    
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    If ValidarDatos Then
        If fCls.Grabar Then
            MsgBox "Registros Guardados"
            Unload Me
        End If
    End If
End Sub

Private Sub Form_Load()
    LlenarComboFrecuencia
    LlenarComboCosto
    IniciarControles
End Sub

Private Sub IniciarControles()
    With fCls.ClsConfigProp
        cboFrecuencia.ListIndex = .TipoProg
        txtDias.Text = .CadaNIntervalo
        txteldia.Text = .Dia
        txtDiasD.Text = .DiasDespachar
        txtDiasR.Text = .DiasReposicion
        txtDiasA.Text = .DiasAnalisis
        cboCosto.ListIndex = .TipoCosto
        Me.chkPromediar.value = IIf(.PromediarVentas, 1, 0)
        Me.chkdescargos.value = IIf(.ConsideraDescargos, 1, 0)
    End With
    LlenarListDepositos
End Sub

Private Function ValidarDiaSemana(pDia As Integer) As Integer
    On Error Resume Next
    ValidarDiaSemana = fCls.ClsConfigProp.ArrayDiasSeleccionados(pDia)
End Function

Private Function ValidarDatos() As Boolean
    With fCls.ClsConfigProp
        If Not ValidarTxt() Then Exit Function
        If Not ExisteDepositoSel() Then
            MsgBox "No existen depositos Seleccionados"
            Exit Function
        End If
        If Not ExisteDepositoPredeterminado() Then
            MsgBox "Debe Seleccionar el Deposito para el que se Despachara la ODC, presione DblClick sobre el Deposito para Seleccionar"
            Exit Function
        End If
        If cboFrecuencia.ListIndex = eOdcAutoDiaria And Val(txtDias.Text) > 7 Then
            MsgBox "El numero de dias no puede ser mayor a 7", vbExclamation
            Exit Function
        ElseIf cboFrecuencia.ListIndex = eOdcAutoSemanal And Val(txtDias.Text) > 4 Then
            MsgBox "El numero de semanas no puede ser mayor a 4", vbExclamation
            Exit Function
        ElseIf cboFrecuencia.ListIndex = eOdcAutoSemanal And Val(txtDias.Text) <= 4 Then
            If Not ValidarDiaSeleccionado Then
                MsgBox "Debe Seleccionar un Dia", vbExclamation
                Exit Function
            End If
        ElseIf cboFrecuencia.ListIndex = eOdcAutoMensual And Val(txtDias.Text) > 12 Then
            MsgBox "El numero de meses no puede ser mayor a 12", vbExclamation
            Exit Function
        End If
        
        .TipoProg = cboFrecuencia.ListIndex
        .TipoCosto = cboCosto.ListIndex
        .CadaNIntervalo = txtDias.Text
        .Dia = txteldia.Text
        .DiasAnalisis = txtDiasA.Text
        .DiasDespachar = txtDiasD.Text
        .DiasReposicion = txtDiasR.Text
        .DiasSel = ObtenerDiasSel(.TipoProg = eOdcAutoSemanal)
        .PromediarVentas = chkPromediar.value = 1
        .ConsideraDescargos = chkdescargos.value = 1
        SeleccionarDepositos
    End With
    ValidarDatos = True
End Function

Private Function LlenarComboFrecuencia()
    Dim i As Integer
    cboFrecuencia.Clear
    For i = eOdcAutoDiaria To eOdcAutoMensual
        cboFrecuencia.AddItem fCls.ClsConfigProp.ObtenerTipoProg(i)
    Next i
    
End Function

Private Function LlenarComboCosto()
    Dim i As Integer
    
    cboCosto.Clear
    For i = etcActual To etcReposicion
        cboCosto.AddItem fCls.ClsConfigProp.ObtenerTipoCosto(i)
    Next i
    
End Function

Private Sub LlenarListDepositos()
    Dim mDep As clsOdcAutoDeposito
    Dim mItm As ListItem
    
    For Each mDep In fCls.ClsConfigProp.Depositos
       Set mItm = Me.lvDeposito.ListItems.add(, , mDep.Descripcion)
       mItm.SubItems(1) = IIf(mDep.Predeterminado, "Despachar", "")
       mItm.Tag = mDep.Codigo
       mItm.Checked = mDep.Seleccionado
    Next
       
End Sub

Private Function ExisteDepositoSel() As Boolean
    Dim mItm As ListItem
    
    For Each mItm In lvDeposito.ListItems
        If mItm.Checked Then
            ExisteDepositoSel = True
            Exit Function
        End If
    Next
            
End Function
Private Function ExisteDepositoPredeterminado() As Boolean
    Dim mItm As ListItem
    
    For Each mItm In lvDeposito.ListItems
        If mItm.Checked And mItm.SubItems(1) <> "" Then
            ExisteDepositoPredeterminado = True
            Exit Function
        End If
    Next
            
End Function

Private Sub SeleccionarDepositos()
    Dim mDep As clsOdcAutoDeposito
    Dim mItm As ListItem
    
    For Each mDep In fCls.ClsConfigProp.Depositos
        Set mItm = Me.lvDeposito.FindItem(mDep.Codigo, lvwTag)
        If Not mItm Is Nothing Then
            mDep.Seleccionado = mItm.Checked
            mDep.Predeterminado = Trim(UCase(mItm.SubItems(1))) <> ""
        Else
            mDep.Seleccionado = False
        End If
    Next
End Sub
Private Function ValidarTxt() As Boolean
    Dim mCtrl As Control
    
    For Each mCtrl In Me.Controls
        If TypeOf mCtrl Is TextBox Then
            If Val(mCtrl.Text) = 0 And mCtrl.Visible Then
                MsgBox "El valor de " & BuscarNombreCtrl(mCtrl.Name) & " no es valido", vbExclamation
                Exit Function
            End If
        End If
    Next
    ValidarTxt = True
End Function

Private Function BuscarNombreCtrl(pCtrlName As String) As String
    Select Case pCtrlName
        Case Me.txtDias.Name
            BuscarNombreCtrl = NombreIntervalo
        Case Me.txtDiasA.Name
            BuscarNombreCtrl = "Dias de Analisis"
        Case Me.txtDiasD.Name
            BuscarNombreCtrl = "Dias de Despacho"
        Case Me.txtDiasR.Name
            BuscarNombreCtrl = "Dias de Reposicion"
        Case Me.txteldia.Name
            BuscarNombreCtrl = "Dia de Ejecucion"
    End Select
End Function

Private Function NombreIntervalo() As String
    Select Case fCls.ClsConfigProp.TipoProg
        Case eOdcAutoDiaria
            NombreIntervalo = " Ejecutar cada n Dias"
        Case eOdcAutoMensual
            NombreIntervalo = " Ejecutar cada n Meses"
        Case Else
            NombreIntervalo = " Ejecutar cada n Semanas"
    End Select
End Function

Private Function ObtenerDiasSel(pEsSemanal) As Integer
    Dim mResultado As Integer
    
    If pEsSemanal Then
        For i = 0 To Check1.Count - 1
            If Check1(i).value Then
                mResultado = mResultado + 2 ^ i
            End If
        Next i
    End If
    ObtenerDiasSel = mResultado
End Function

Private Function BuscarDescriLabel(pTipo As Integer) As String
    Select Case pTipo
        Case eOdcAutoDiaria
            BuscarDescriLabel = "Dia(s)"
        Case eOdcAutoMensual
            BuscarDescriLabel = "Mes(es)"
        Case Else
            BuscarDescriLabel = "Semana(s)"
    End Select
End Function

Private Function ValidarKeyascii(pKeyascii As Integer) As Integer
    Select Case pKeyascii
        Case 48 To 57
            ValidarKeyascii = pKeyascii
        Case vbKeyBack
            ValidarKeyascii = pKeyascii
        Case Else
    End Select
End Function


Private Sub lvDeposito_DblClick()
    If Not lvDeposito.SelectedItem Is Nothing Then
        If lvDeposito.SelectedItem.Checked Then
            lvDeposito.SelectedItem.SubItems(1) = IIf(lvDeposito.SelectedItem.SubItems(1) = "", "Despachar", "")
            DesmarcarSeleccionados lvDeposito.SelectedItem
        End If
    End If
End Sub

Private Sub lvDeposito_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    If Not Item.Checked Then
        Item.SubItems(1) = ""
    End If
End Sub

Private Sub txtDias_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidarKeyascii(KeyAscii)
End Sub

Private Sub txtDiasA_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidarKeyascii(KeyAscii)
End Sub

Private Sub txtDiasD_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidarKeyascii(KeyAscii)
End Sub

Private Sub txtDiasR_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidarKeyascii(KeyAscii)
End Sub

Private Sub txteldia_KeyPress(KeyAscii As Integer)
    KeyAscii = ValidarKeyascii(KeyAscii)
End Sub

Private Function ValidarDiaSeleccionado() As Boolean
    For i = 0 To Check1.Count - 1
        If Check1(i).value = 1 Then
            ValidarDiaSeleccionado = True
            Exit Function
        End If
    Next i
    
End Function

Private Sub DesmarcarSeleccionados(piTm As ListItem)
    Dim mItm As ListItem
    
    'solo desmarco si el itm sel paso a predeterminado
    If piTm.SubItems(1) <> "" Then
        For Each mItm In Me.lvDeposito.ListItems
            If mItm.Tag <> piTm.Tag Then
                mItm.SubItems(1) = ""
            End If
        Next
    End If
End Sub
